import gi
import os

gi.require_version("Gtk", "3.0")
gi.require_version('Gdk', '3.0')

from gi.repository import Gtk
from gi.repository import Gdk

settedPiece = ""
settedPieceName = ""
setted2Piece = ""
setted2PieceName = ""
breakPiece = ""
whiteTurn = True
movement = False
colorLight = Gdk.RGBA(0.4,0.4,0.4,1.0)
colorDark = Gdk.RGBA(0.6,0.6,0.6,1.0)
colorSelected = Gdk.RGBA(0.2,0.2,0.8,1.0)
color2nd = Gdk.RGBA(0.8,0.2,0.2,1.0)
isWhiteKingMove = False
isBlackKingMove = False
isWhiteRookMove = False
isBlackRookMove = False
chess = False
spaces = []
numberOfChess = 0
whoStuck = ""
whoStucked = ""
whoChess = ""
kingWayDirection = ""
stuckWayDirection = ""
is2nd = True

def findKingWay():
    global kingWayDirection
    global stuckWayDirection
    for space in spaces:
        space.wayOfKing = False
        space.wayOfStuck = False
        space.wayOfStuck1Rook = False
        space.wayOfStuck2Rook = False
        space.wayOfStuck1Knight = False
        space.wayOfStuck2Knight = False
        space.wayOfStuck1Bishop = False
        space.wayOfStuck2Bishop = False
        space.wayOfStuckQueen = False
    if(whoChess == "whiterook1" and kingWayDirection  == "top"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can1WhiteRookAttackTop
    if(whoStuck == "whiterook1" and stuckWayDirection == "top"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can1WhiteRookAttackTop or space.can1WhiteRook2ndAttackTop
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can1WhiteRookAttackTop or space.can1WhiteRook2ndAttackTop
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can1WhiteRookAttackTop or space.can1WhiteRook2ndAttackTop
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can1WhiteRookAttackTop or space.can1WhiteRook2ndAttackTop
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can1WhiteRookAttackTop or space.can1WhiteRook2ndAttackTop
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can1WhiteRookAttackTop or space.can1WhiteRook2ndAttackTop
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can1WhiteRookAttackTop or space.can1WhiteRook2ndAttackTop
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can1WhiteRookAttackTop or space.can1WhiteRook2ndAttackTop
    if(whoChess == "whiterook1" and kingWayDirection  == "bottom"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can1WhiteRookAttackBottom
    if(whoStuck == "whiterook1" and stuckWayDirection == "bottom"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can1WhiteRookAttackBottom or space.can1WhiteRook2ndAttackBottom
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can1WhiteRookAttackBottom or space.can1WhiteRook2ndAttackBottom
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can1WhiteRookAttackBottom or space.can1WhiteRook2ndAttackBottom
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can1WhiteRookAttackBottom or space.can1WhiteRook2ndAttackBottom
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can1WhiteRookAttackBottom or space.can1WhiteRook2ndAttackBottom
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can1WhiteRookAttackBottom or space.can1WhiteRook2ndAttackBottom
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can1WhiteRookAttackBottom or space.can1WhiteRook2ndAttackBottom
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can1WhiteRookAttackBottom or space.can1WhiteRook2ndAttackBottom
    if(whoChess == "whiterook1" and kingWayDirection  == "right"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can1WhiteRookAttackRight
    if(whoStuck == "whiterook1" and stuckWayDirection == "right"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can1WhiteRookAttackRight or space.can1WhiteRook2ndAttackRight
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can1WhiteRookAttackRight or space.can1WhiteRook2ndAttackRight
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can1WhiteRookAttackRight or space.can1WhiteRook2ndAttackRight
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can1WhiteRookAttackRight or space.can1WhiteRook2ndAttackRight
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can1WhiteRookAttackRight or space.can1WhiteRook2ndAttackRight
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can1WhiteRookAttackRight or space.can1WhiteRook2ndAttackRight
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can1WhiteRookAttackRight or space.can1WhiteRook2ndAttackRight
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can1WhiteRookAttackRight or space.can1WhiteRook2ndAttackRight
    if(whoChess == "whiterook1" and kingWayDirection  == "left"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can1WhiteRookAttackLeft
    if(whoStuck == "whiterook1" and stuckWayDirection == "left"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can1WhiteRookAttackLeft or space.can1WhiteRook2ndAttackLeft
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can1WhiteRookAttackLeft or space.can1WhiteRook2ndAttackLeft
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can1WhiteRookAttackLeft or space.can1WhiteRook2ndAttackLeft
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can1WhiteRookAttackLeft or space.can1WhiteRook2ndAttackLeft
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can1WhiteRookAttackLeft or space.can1WhiteRook2ndAttackLeft
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can1WhiteRookAttackLeft or space.can1WhiteRook2ndAttackLeft
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can1WhiteRookAttackLeft or space.can1WhiteRook2ndAttackLeft
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can1WhiteRookAttackLeft or space.can1WhiteRook2ndAttackLeft
    if(whoChess == "whiterook2" and kingWayDirection  == "top"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can2WhiteRookAttackTop
    if(whoStuck == "whiterook2" and stuckWayDirection == "top"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can2WhiteRookAttackTop or space.can2WhiteRook2ndAttackTop
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can2WhiteRookAttackTop or space.can2WhiteRook2ndAttackTop
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can2WhiteRookAttackTop or space.can2WhiteRook2ndAttackTop
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can2WhiteRookAttackTop or space.can2WhiteRook2ndAttackTop
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can2WhiteRookAttackTop or space.can2WhiteRook2ndAttackTop
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can2WhiteRookAttackTop or space.can2WhiteRook2ndAttackTop
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can2WhiteRookAttackTop or space.can2WhiteRook2ndAttackTop
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can2WhiteRookAttackTop or space.can2WhiteRook2ndAttackTop
    if(whoChess == "whiterook2" and kingWayDirection  == "bottom"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can2WhiteRookAttackBottom
    if(whoStuck == "whiterook2" and stuckWayDirection == "bottom"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can2WhiteRookAttackBottom or space.can2WhiteRook2ndAttackBottom
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can2WhiteRookAttackBottom or space.can2WhiteRook2ndAttackBottom
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can2WhiteRookAttackBottom or space.can2WhiteRook2ndAttackBottom
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can2WhiteRookAttackBottom or space.can2WhiteRook2ndAttackBottom
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can2WhiteRookAttackBottom or space.can2WhiteRook2ndAttackBottom
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can2WhiteRookAttackBottom or space.can2WhiteRook2ndAttackBottom
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can2WhiteRookAttackBottom or space.can2WhiteRook2ndAttackBottom
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can2WhiteRookAttackBottom or space.can2WhiteRook2ndAttackBottom
    if(whoChess == "whiterook2" and kingWayDirection  == "right"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can2WhiteRookAttackRight
    if(whoStuck == "whiterook2" and stuckWayDirection == "right"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can2WhiteRookAttackRight or space.can2WhiteRook2ndAttackRight
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can2WhiteRookAttackRight or space.can2WhiteRook2ndAttackRight
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can2WhiteRookAttackRight or space.can2WhiteRook2ndAttackRight
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can2WhiteRookAttackRight or space.can2WhiteRook2ndAttackRight
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can2WhiteRookAttackRight or space.can2WhiteRook2ndAttackRight
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can2WhiteRookAttackRight or space.can2WhiteRook2ndAttackRight
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can2WhiteRookAttackRight or space.can2WhiteRook2ndAttackRight
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can2WhiteRookAttackRight or space.can2WhiteRook2ndAttackRight
    if(whoChess == "whiterook2" and kingWayDirection  == "left"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can2WhiteRookAttackLeft
    if(whoStuck == "whiterook2" and stuckWayDirection == "left"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can2WhiteRookAttackLeft or space.can2WhiteRook2ndAttackLeft
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can2WhiteRookAttackLeft or space.can2WhiteRook2ndAttackLeft
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can2WhiteRookAttackLeft or space.can2WhiteRook2ndAttackLeft
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can2WhiteRookAttackLeft or space.can2WhiteRook2ndAttackLeft
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can2WhiteRookAttackLeft or space.can2WhiteRook2ndAttackLeft
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can2WhiteRookAttackLeft or space.can2WhiteRook2ndAttackLeft
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can2WhiteRookAttackLeft or space.can2WhiteRook2ndAttackLeft
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can2WhiteRookAttackLeft or space.can2WhiteRook2ndAttackLeft
    if(whoChess == "blackrook1" and kingWayDirection  == "top"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can1BlackRookAttackTop
    if(whoStuck == "blackrook1" and stuckWayDirection == "top"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can1BlackRookAttackTop or space.can1BlackRook2ndAttackTop
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can1BlackRookAttackTop or space.can1BlackRook2ndAttackTop
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can1BlackRookAttackTop or space.can1BlackRook2ndAttackTop
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can1BlackRookAttackTop or space.can1BlackRook2ndAttackTop
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can1BlackRookAttackTop or space.can1BlackRook2ndAttackTop
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can1BlackRookAttackTop or space.can1BlackRook2ndAttackTop
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can1BlackRookAttackTop or space.can1BlackRook2ndAttackTop
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can1BlackRookAttackTop or space.can1BlackRook2ndAttackTop
    if(whoChess == "blackrook1" and kingWayDirection  == "bottom"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can1BlackRookAttackBottom
    if(whoStuck == "blackrook1" and stuckWayDirection == "bottom"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can1BlackRookAttackBottom or space.can1BlackRook2ndAttackBottom
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can1BlackRookAttackBottom or space.can1BlackRook2ndAttackBottom
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can1BlackRookAttackBottom or space.can1BlackRook2ndAttackBottom
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can1BlackRookAttackBottom or space.can1BlackRook2ndAttackBottom
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can1BlackRookAttackBottom or space.can1BlackRook2ndAttackBottom
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can1BlackRookAttackBottom or space.can1BlackRook2ndAttackBottom
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can1BlackRookAttackBottom or space.can1BlackRook2ndAttackBottom
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can1BlackRookAttackBottom or space.can1BlackRook2ndAttackBottom
    if(whoChess == "blackrook1" and kingWayDirection  == "right"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can1BlackRookAttackRight
    if(whoStuck == "blackrook1" and stuckWayDirection == "right"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can1BlackRookAttackRight or space.can1BlackRook2ndAttackRight
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can1BlackRookAttackRight or space.can1BlackRook2ndAttackRight
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can1BlackRookAttackRight or space.can1BlackRook2ndAttackRight
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can1BlackRookAttackRight or space.can1BlackRook2ndAttackRight
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can1BlackRookAttackRight or space.can1BlackRook2ndAttackRight
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can1BlackRookAttackRight or space.can1BlackRook2ndAttackRight
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can1BlackRookAttackRight or space.can1BlackRook2ndAttackRight
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can1BlackRookAttackRight or space.can1BlackRook2ndAttackRight
    if(whoChess == "blackrook1" and kingWayDirection  == "left"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can1BlackRookAttackLeft
    if(whoStuck == "blackrook1" and stuckWayDirection == "left"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can1BlackRookAttackLeft or space.can1BlackRook2ndAttackLeft
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can1BlackRookAttackLeft or space.can1BlackRook2ndAttackLeft
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can1BlackRookAttackLeft or space.can1BlackRook2ndAttackLeft
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can1BlackRookAttackLeft or space.can1BlackRook2ndAttackLeft
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can1BlackRookAttackLeft or space.can1BlackRook2ndAttackLeft
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can1BlackRookAttackLeft or space.can1BlackRook2ndAttackLeft
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can1BlackRookAttackLeft or space.can1BlackRook2ndAttackLeft
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can1BlackRookAttackLeft or space.can1BlackRook2ndAttackLeft
    if(whoChess == "blackrook2" and kingWayDirection  == "top"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can2BlackRookAttackTop
    if(whoStuck == "blackrook2" and stuckWayDirection == "top"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can2BlackRookAttackTop or space.can2BlackRook2ndAttackTop
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can2BlackRookAttackTop or space.can2BlackRook2ndAttackTop
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can2BlackRookAttackTop or space.can2BlackRook2ndAttackTop
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can2BlackRookAttackTop or space.can2BlackRook2ndAttackTop
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can2BlackRookAttackTop or space.can2BlackRook2ndAttackTop
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can2BlackRookAttackTop or space.can2BlackRook2ndAttackTop
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can2BlackRookAttackTop or space.can2BlackRook2ndAttackTop
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can2BlackRookAttackTop or space.can2BlackRook2ndAttackTop
    if(whoChess == "blackrook2" and kingWayDirection  == "bottom"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can2BlackRookAttackBottom
    if(whoStuck == "blackrook2" and stuckWayDirection == "bottom"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can2BlackRookAttackBottom or space.can2BlackRook2ndAttackBottom
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can2BlackRookAttackBottom or space.can2BlackRook2ndAttackBottom
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can2BlackRookAttackBottom or space.can2BlackRook2ndAttackBottom
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can2BlackRookAttackBottom or space.can2BlackRook2ndAttackBottom
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can2BlackRookAttackBottom or space.can2BlackRook2ndAttackBottom
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can2BlackRookAttackBottom or space.can2BlackRook2ndAttackBottom
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can2BlackRookAttackBottom or space.can2BlackRook2ndAttackBottom
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can2BlackRookAttackBottom or space.can2BlackRook2ndAttackBottom
    if(whoChess == "blackrook2" and kingWayDirection  == "right"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can2BlackRookAttackRight
    if(whoStuck == "blackrook2" and stuckWayDirection == "right"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can2BlackRookAttackRight or space.can2BlackRook2ndAttackRight
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can2BlackRookAttackRight or space.can2BlackRook2ndAttackRight
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can2BlackRookAttackRight or space.can2BlackRook2ndAttackRight
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can2BlackRookAttackRight or space.can2BlackRook2ndAttackRight
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can2BlackRookAttackRight or space.can2BlackRook2ndAttackRight
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can2BlackRookAttackRight or space.can2BlackRook2ndAttackRight
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can2BlackRookAttackRight or space.can2BlackRook2ndAttackRight
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can2BlackRookAttackRight or space.can2BlackRook2ndAttackRight
    if(whoChess == "blackrook2" and kingWayDirection  == "left"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can2BlackRookAttackLeft
    if(whoStuck == "blackrook2" and stuckWayDirection == "left"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can2BlackRookAttackLeft or space.can2BlackRook2ndAttackLeft
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can2BlackRookAttackLeft or space.can2BlackRook2ndAttackLeft
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can2BlackRookAttackLeft or space.can2BlackRook2ndAttackLeft
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can2BlackRookAttackLeft or space.can2BlackRook2ndAttackLeft
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can2BlackRookAttackLeft or space.can2BlackRook2ndAttackLeft
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can2BlackRookAttackLeft or space.can2BlackRook2ndAttackLeft
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can2BlackRookAttackLeft or space.can2BlackRook2ndAttackLeft
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can2BlackRookAttackLeft or space.can2BlackRook2ndAttackLeft
    if(whoChess == "whitebishop1" and kingWayDirection  == "top"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can1WhiteBishopAttackTop
    if(whoStuck == "whitebishop1" and stuckWayDirection == "top"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can1WhiteBishopAttackTop or space.can1WhiteBishop2ndAttackTop
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can1WhiteBishopAttackTop or space.can1WhiteBishop2ndAttackTop
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can1WhiteBishopAttackTop or space.can1WhiteBishop2ndAttackTop
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can1WhiteBishopAttackTop or space.can1WhiteBishop2ndAttackTop
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can1WhiteBishopAttackTop or space.can1WhiteBishop2ndAttackTop
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can1WhiteBishopAttackTop or space.can1WhiteBishop2ndAttackTop
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can1WhiteBishopAttackTop or space.can1WhiteBishop2ndAttackTop
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can1WhiteBishopAttackTop or space.can1WhiteBishop2ndAttackTop
    if(whoChess == "whitebishop1" and kingWayDirection  == "bottom"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can1WhiteBishopAttackBottom
    if(whoStuck == "whitebishop1" and stuckWayDirection == "bottom"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can1WhiteBishopAttackBottom or space.can1WhiteBishop2ndAttackBottom
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can1WhiteBishopAttackBottom or space.can1WhiteBishop2ndAttackBottom
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can1WhiteBishopAttackBottom or space.can1WhiteBishop2ndAttackBottom
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can1WhiteBishopAttackBottom or space.can1WhiteBishop2ndAttackBottom
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can1WhiteBishopAttackBottom or space.can1WhiteBishop2ndAttackBottom
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can1WhiteBishopAttackBottom or space.can1WhiteBishop2ndAttackBottom
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can1WhiteBishopAttackBottom or space.can1WhiteBishop2ndAttackBottom
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can1WhiteBishopAttackBottom or space.can1WhiteBishop2ndAttackBottom
    if(whoChess == "whitebishop1" and kingWayDirection  == "right"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can1WhiteBishopAttackRight
    if(whoStuck == "whitebishop1" and stuckWayDirection == "right"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can1WhiteBishopAttackRight or space.can1WhiteBishop2ndAttackRight
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can1WhiteBishopAttackRight or space.can1WhiteBishop2ndAttackRight
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can1WhiteBishopAttackRight or space.can1WhiteBishop2ndAttackRight
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can1WhiteBishopAttackRight or space.can1WhiteBishop2ndAttackRight
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can1WhiteBishopAttackRight or space.can1WhiteBishop2ndAttackRight
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can1WhiteBishopAttackRight or space.can1WhiteBishop2ndAttackRight
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can1WhiteBishopAttackRight or space.can1WhiteBishop2ndAttackRight
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can1WhiteBishopAttackRight or space.can1WhiteBishop2ndAttackRight
    if(whoChess == "whitebishop1" and kingWayDirection  == "left"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can1WhiteBishopAttackLeft
    if(whoStuck == "whitebishop1" and stuckWayDirection == "left"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can1WhiteBishopAttackLeft or space.can1WhiteBishop2ndAttackLeft
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can1WhiteBishopAttackLeft or space.can1WhiteBishop2ndAttackLeft
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can1WhiteBishopAttackLeft or space.can1WhiteBishop2ndAttackLeft
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can1WhiteBishopAttackLeft or space.can1WhiteBishop2ndAttackLeft
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can1WhiteBishopAttackLeft or space.can1WhiteBishop2ndAttackLeft
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can1WhiteBishopAttackLeft or space.can1WhiteBishop2ndAttackLeft
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can1WhiteBishopAttackLeft or space.can1WhiteBishop2ndAttackLeft
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can1WhiteBishopAttackLeft or space.can1WhiteBishop2ndAttackLeft
    if(whoChess == "whitebishop2" and kingWayDirection  == "top"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can2WhiteBishopAttackTop
    if(whoStuck == "whitebishop2" and stuckWayDirection == "top"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can2WhiteBishopAttackTop or space.can2WhiteBishop2ndAttackTop
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can2WhiteBishopAttackTop or space.can2WhiteBishop2ndAttackTop
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can2WhiteBishopAttackTop or space.can2WhiteBishop2ndAttackTop
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can2WhiteBishopAttackTop or space.can2WhiteBishop2ndAttackTop
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can2WhiteBishopAttackTop or space.can2WhiteBishop2ndAttackTop
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can2WhiteBishopAttackTop or space.can2WhiteBishop2ndAttackTop
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can2WhiteBishopAttackTop or space.can2WhiteBishop2ndAttackTop
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can2WhiteBishopAttackTop or space.can2WhiteBishop2ndAttackTop
    if(whoChess == "whitebishop2" and kingWayDirection  == "bottom"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can2WhiteBishopAttackBottom
    if(whoStuck == "whitebishop2" and stuckWayDirection == "bottom"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can2WhiteBishopAttackBottom or space.can2WhiteBishop2ndAttackBottom
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can2WhiteBishopAttackBottom or space.can2WhiteBishop2ndAttackBottom
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can2WhiteBishopAttackBottom or space.can2WhiteBishop2ndAttackBottom
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can2WhiteBishopAttackBottom or space.can2WhiteBishop2ndAttackBottom
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can2WhiteBishopAttackBottom or space.can2WhiteBishop2ndAttackBottom
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can2WhiteBishopAttackBottom or space.can2WhiteBishop2ndAttackBottom
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can2WhiteBishopAttackBottom or space.can2WhiteBishop2ndAttackBottom
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can2WhiteBishopAttackBottom or space.can2WhiteBishop2ndAttackBottom
    if(whoChess == "whitebishop2" and kingWayDirection  == "right"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can2WhiteBishopAttackRight
    if(whoStuck == "whitebishop2" and stuckWayDirection == "right"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can2WhiteBishopAttackRight or space.can2WhiteBishop2ndAttackRight
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can2WhiteBishopAttackRight or space.can2WhiteBishop2ndAttackRight
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can2WhiteBishopAttackRight or space.can2WhiteBishop2ndAttackRight
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can2WhiteBishopAttackRight or space.can2WhiteBishop2ndAttackRight
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can2WhiteBishopAttackRight or space.can2WhiteBishop2ndAttackRight
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can2WhiteBishopAttackRight or space.can2WhiteBishop2ndAttackRight
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can2WhiteBishopAttackRight or space.can2WhiteBishop2ndAttackRight
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can2WhiteBishopAttackRight or space.can2WhiteBishop2ndAttackRight
    if(whoChess == "whitebishop2" and kingWayDirection  == "left"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can2WhiteBishopAttackLeft
    if(whoStuck == "whitebishop2" and stuckWayDirection == "left"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can2WhiteBishopAttackLeft or space.can2WhiteBishop2ndAttackLeft
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can2WhiteBishopAttackLeft or space.can2WhiteBishop2ndAttackLeft
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can2WhiteBishopAttackLeft or space.can2WhiteBishop2ndAttackLeft
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can2WhiteBishopAttackLeft or space.can2WhiteBishop2ndAttackLeft
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can2WhiteBishopAttackLeft or space.can2WhiteBishop2ndAttackLeft
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can2WhiteBishopAttackLeft or space.can2WhiteBishop2ndAttackLeft
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can2WhiteBishopAttackLeft or space.can2WhiteBishop2ndAttackLeft
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can2WhiteBishopAttackLeft or space.can2WhiteBishop2ndAttackLeft
    if(whoChess == "blackbishop1" and kingWayDirection  == "top"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can1BlackBishopAttackTop
    if(whoStuck == "blackbishop1" and stuckWayDirection == "top"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can1BlackBishopAttackTop or space.can1BlackBishop2ndAttackTop
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can1BlackBishopAttackTop or space.can1BlackBishop2ndAttackTop
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can1BlackBishopAttackTop or space.can1BlackBishop2ndAttackTop
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can1BlackBishopAttackTop or space.can1BlackBishop2ndAttackTop
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can1BlackBishopAttackTop or space.can1BlackBishop2ndAttackTop
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can1BlackBishopAttackTop or space.can1BlackBishop2ndAttackTop
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can1BlackBishopAttackTop or space.can1BlackBishop2ndAttackTop
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can1BlackBishopAttackTop or space.can1BlackBishop2ndAttackTop
    if(whoChess == "blackbishop1" and kingWayDirection  == "bottom"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can1BlackBishopAttackBottom
    if(whoStuck == "blackbishop1" and stuckWayDirection == "bottom"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can1BlackBishopAttackBottom or space.can1BlackBishop2ndAttackBottom
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can1BlackBishopAttackBottom or space.can1BlackBishop2ndAttackBottom
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can1BlackBishopAttackBottom or space.can1BlackBishop2ndAttackBottom
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can1BlackBishopAttackBottom or space.can1BlackBishop2ndAttackBottom
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can1BlackBishopAttackBottom or space.can1BlackBishop2ndAttackBottom
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can1BlackBishopAttackBottom or space.can1BlackBishop2ndAttackBottom
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can1BlackBishopAttackBottom or space.can1BlackBishop2ndAttackBottom
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can1BlackBishopAttackBottom or space.can1BlackBishop2ndAttackBottom
    if(whoChess == "blackbishop1" and kingWayDirection  == "right"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can1BlackBishopAttackRight
    if(whoStuck == "blackbishop1" and stuckWayDirection == "right"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can1BlackBishopAttackRight or space.can1BlackBishop2ndAttackRight
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can1BlackBishopAttackRight or space.can1BlackBishop2ndAttackRight
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can1BlackBishopAttackRight or space.can1BlackBishop2ndAttackRight
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can1BlackBishopAttackRight or space.can1BlackBishop2ndAttackRight
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can1BlackBishopAttackRight or space.can1BlackBishop2ndAttackRight
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can1BlackBishopAttackRight or space.can1BlackBishop2ndAttackRight
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can1BlackBishopAttackRight or space.can1BlackBishop2ndAttackRight
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can1BlackBishopAttackRight or space.can1BlackBishop2ndAttackRight
    if(whoChess == "blackbishop1" and kingWayDirection  == "left"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can1BlackBishopAttackLeft
    if(whoStuck == "blackbishop1" and stuckWayDirection == "left"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can1BlackBishopAttackLeft or space.can1BlackBishop2ndAttackLeft
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can1BlackBishopAttackLeft or space.can1BlackBishop2ndAttackLeft
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can1BlackBishopAttackLeft or space.can1BlackBishop2ndAttackLeft
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can1BlackBishopAttackLeft or space.can1BlackBishop2ndAttackLeft
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can1BlackBishopAttackLeft or space.can1BlackBishop2ndAttackLeft
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can1BlackBishopAttackLeft or space.can1BlackBishop2ndAttackLeft
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can1BlackBishopAttackLeft or space.can1BlackBishop2ndAttackLeft
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can1BlackBishopAttackLeft or space.can1BlackBishop2ndAttackLeft
    if(whoChess == "blackbishop2" and kingWayDirection  == "top"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can2BlackBishopAttackTop
    if(whoStuck == "blackbishop2" and stuckWayDirection == "top"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can2BlackBishopAttackTop or space.can2BlackBishop2ndAttackTop
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can2BlackBishopAttackTop or space.can2BlackBishop2ndAttackTop
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can2BlackBishopAttackTop or space.can2BlackBishop2ndAttackTop
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can2BlackBishopAttackTop or space.can2BlackBishop2ndAttackTop
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can2BlackBishopAttackTop or space.can2BlackBishop2ndAttackTop
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can2BlackBishopAttackTop or space.can2BlackBishop2ndAttackTop
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can2BlackBishopAttackTop or space.can2BlackBishop2ndAttackTop
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can2BlackBishopAttackTop or space.can2BlackBishop2ndAttackTop
    if(whoChess == "blackbishop2" and kingWayDirection  == "bottom"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can2BlackBishopAttackBottom
    if(whoStuck == "blackbishop2" and stuckWayDirection == "bottom"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can2BlackBishopAttackBottom or space.can2BlackBishop2ndAttackBottom
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can2BlackBishopAttackBottom or space.can2BlackBishop2ndAttackBottom
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can2BlackBishopAttackBottom or space.can2BlackBishop2ndAttackBottom
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can2BlackBishopAttackBottom or space.can2BlackBishop2ndAttackBottom
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can2BlackBishopAttackBottom or space.can2BlackBishop2ndAttackBottom
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can2BlackBishopAttackBottom or space.can2BlackBishop2ndAttackBottom
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can2BlackBishopAttackBottom or space.can2BlackBishop2ndAttackBottom
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can2BlackBishopAttackBottom or space.can2BlackBishop2ndAttackBottom
    if(whoChess == "blackbishop2" and kingWayDirection  == "right"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can2BlackBishopAttackRight
    if(whoStuck == "blackbishop2" and stuckWayDirection == "right"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can2BlackBishopAttackRight or space.can2BlackBishop2ndAttackRight
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can2BlackBishopAttackRight or space.can2BlackBishop2ndAttackRight
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can2BlackBishopAttackRight or space.can2BlackBishop2ndAttackRight
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can2BlackBishopAttackRight or space.can2BlackBishop2ndAttackRight
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can2BlackBishopAttackRight or space.can2BlackBishop2ndAttackRight
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can2BlackBishopAttackRight or space.can2BlackBishop2ndAttackRight
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can2BlackBishopAttackRight or space.can2BlackBishop2ndAttackRight
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can2BlackBishopAttackRight or space.can2BlackBishop2ndAttackRight
    if(whoChess == "blackbishop2" and kingWayDirection  == "left"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.can2BlackBishopAttackLeft
    if(whoStuck == "blackbishop2" and stuckWayDirection == "left"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.can2BlackBishopAttackLeft or space.can2BlackBishop2ndAttackLeft
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.can2BlackBishopAttackLeft or space.can2BlackBishop2ndAttackLeft
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.can2BlackBishopAttackLeft or space.can2BlackBishop2ndAttackLeft
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.can2BlackBishopAttackLeft or space.can2BlackBishop2ndAttackLeft
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.can2BlackBishopAttackLeft or space.can2BlackBishop2ndAttackLeft
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.can2BlackBishopAttackLeft or space.can2BlackBishop2ndAttackLeft
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.can2BlackBishopAttackLeft or space.can2BlackBishop2ndAttackLeft
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.can2BlackBishopAttackLeft or space.can2BlackBishop2ndAttackLeft
    if(whoChess == "whitequeen" and kingWayDirection  == "top"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.canWhiteQueenAttackTop
    if(whoStuck == "whitequeen" and stuckWayDirection == "top"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.canWhiteQueenAttackTop or space.canWhiteQueen2ndAttackTop
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.canWhiteQueenAttackTop or space.canWhiteQueen2ndAttackTop
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.canWhiteQueenAttackTop or space.canWhiteQueen2ndAttackTop
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.canWhiteQueenAttackTop or space.canWhiteQueen2ndAttackTop
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.canWhiteQueenAttackTop or space.canWhiteQueen2ndAttackTop
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.canWhiteQueenAttackTop or space.canWhiteQueen2ndAttackTop
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.canWhiteQueenAttackTop or space.canWhiteQueen2ndAttackTop
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.canWhiteQueenAttackTop or space.canWhiteQueen2ndAttackTop
    if(whoChess == "whitequeen" and kingWayDirection  == "bottom"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.canWhiteQueenAttackBottom
    if(whoStuck == "whitequeen" and stuckWayDirection == "bottom"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.canWhiteQueenAttackBottom or space.canWhiteQueen2ndAttackBottom
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.canWhiteQueenAttackBottom or space.canWhiteQueen2ndAttackBottom
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.canWhiteQueenAttackBottom or space.canWhiteQueen2ndAttackBottom
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.canWhiteQueenAttackBottom or space.canWhiteQueen2ndAttackBottom
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.canWhiteQueenAttackBottom or space.canWhiteQueen2ndAttackBottom
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.canWhiteQueenAttackBottom or space.canWhiteQueen2ndAttackBottom
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.canWhiteQueenAttackBottom or space.canWhiteQueen2ndAttackBottom
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.canWhiteQueenAttackBottom or space.canWhiteQueen2ndAttackBottom
    if(whoChess == "whitequeen" and kingWayDirection  == "right"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.canWhiteQueenAttackRight
    if(whoStuck == "whitequeen" and stuckWayDirection == "right"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.canWhiteQueenAttackRight or space.canWhiteQueen2ndAttackRight
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.canWhiteQueenAttackRight or space.canWhiteQueen2ndAttackRight
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.canWhiteQueenAttackRight or space.canWhiteQueen2ndAttackRight
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.canWhiteQueenAttackRight or space.canWhiteQueen2ndAttackRight
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.canWhiteQueenAttackRight or space.canWhiteQueen2ndAttackRight
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.canWhiteQueenAttackRight or space.canWhiteQueen2ndAttackRight
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.canWhiteQueenAttackRight or space.canWhiteQueen2ndAttackRight
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.canWhiteQueenAttackRight or space.canWhiteQueen2ndAttackRight
    if(whoChess == "whitequeen" and kingWayDirection  == "left"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.canWhiteQueenAttackLeft
    if(whoStuck == "whitequeen" and stuckWayDirection == "left"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.canWhiteQueenAttackLeft or space.canWhiteQueen2ndAttackLeft
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.canWhiteQueenAttackLeft or space.canWhiteQueen2ndAttackLeft
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.canWhiteQueenAttackLeft or space.canWhiteQueen2ndAttackLeft
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.canWhiteQueenAttackLeft or space.canWhiteQueen2ndAttackLeft
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.canWhiteQueenAttackLeft or space.canWhiteQueen2ndAttackLeft
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.canWhiteQueenAttackLeft or space.canWhiteQueen2ndAttackLeft
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.canWhiteQueenAttackLeft or space.canWhiteQueen2ndAttackLeft
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.canWhiteQueenAttackLeft or space.canWhiteQueen2ndAttackLeft
    if(whoChess == "blackqueen" and kingWayDirection  == "top"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.canBlackQueenAttackTop
    if(whoStuck == "blackqueen" and stuckWayDirection == "top"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.canBlackQueenAttackTop or space.canBlackQueen2ndAttackTop
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.canBlackQueenAttackTop or space.canBlackQueen2ndAttackTop
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.canBlackQueenAttackTop or space.canBlackQueen2ndAttackTop
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.canBlackQueenAttackTop or space.canBlackQueen2ndAttackTop
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.canBlackQueenAttackTop or space.canBlackQueen2ndAttackTop
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.canBlackQueenAttackTop or space.canBlackQueen2ndAttackTop
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.canBlackQueenAttackTop or space.canBlackQueen2ndAttackTop
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.canBlackQueenAttackTop or space.canBlackQueen2ndAttackTop
    if(whoChess == "blackqueen" and kingWayDirection  == "bottom"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.canBlackQueenAttackBottom
    if(whoStuck == "blackqueen" and stuckWayDirection == "bottom"):
        for space in spaces:
            if ("queen" in whoStucked):
                space.wayOfStuck1Rook = space.canBlackQueenAttackBottom or space.canBlackQueen2ndAttackBottom
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.canBlackQueenAttackBottom or space.canBlackQueen2ndAttackBottom
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.canBlackQueenAttackBottom or space.canBlackQueen2ndAttackBottom
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.canBlackQueenAttackBottom or space.canBlackQueen2ndAttackBottom
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.canBlackQueenAttackBottom or space.canBlackQueen2ndAttackBottom
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.canBlackQueenAttackBottom or space.canBlackQueen2ndAttackBottom
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.canBlackQueenAttackBottom or space.canBlackQueen2ndAttackBottom
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.canBlackQueenAttackBottom or space.canBlackQueen2ndAttackBottom
    if(whoChess == "blackqueen" and kingWayDirection  == "right"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.canBlackQueenAttackRight
    if(whoStuck == "blackqueen" and stuckWayDirection == "right"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.canBlackQueenAttackRight or space.canBlackQueen2ndAttackRight
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.canBlackQueenAttackRight or space.canBlackQueen2ndAttackRight
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.canBlackQueenAttackRight or space.canBlackQueen2ndAttackRight
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.canBlackQueenAttackRight or space.canBlackQueen2ndAttackRight
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.canBlackQueenAttackRight or space.canBlackQueen2ndAttackRight
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.canBlackQueenAttackRight or space.canBlackQueen2ndAttackRight
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.canBlackQueenAttackRight or space.canBlackQueen2ndAttackRight
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.canBlackQueenAttackRight or space.canBlackQueen2ndAttackRight
    if(whoChess == "blackqueen" and kingWayDirection  == "left"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.canBlackQueenAttackLeft
    if(whoStuck == "blackqueen" and stuckWayDirection == "left"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.canBlackQueenAttackLeft or space.canBlackQueen2ndAttackLeft
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.canBlackQueenAttackLeft or space.canBlackQueen2ndAttackLeft
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.canBlackQueenAttackLeft or space.canBlackQueen2ndAttackLeft
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.canBlackQueenAttackLeft or space.canBlackQueen2ndAttackLeft
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.canBlackQueenAttackLeft or space.canBlackQueen2ndAttackLeft
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.canBlackQueenAttackLeft or space.canBlackQueen2ndAttackLeft
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.canBlackQueenAttackLeft or space.canBlackQueen2ndAttackLeft
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.canBlackQueenAttackLeft or space.canBlackQueen2ndAttackLeft
    if(whoChess == "whitequeen" and kingWayDirection  == "topright"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.canWhiteQueenAttackTopRight
    if(whoStuck == "whitequeen" and stuckWayDirection == "topright"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.canWhiteQueenAttackTopRight or space.canWhiteQueen2ndAttackTopRight
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.canWhiteQueenAttackTopRight or space.canWhiteQueen2ndAttackTopRight
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.canWhiteQueenAttackTopRight or space.canWhiteQueen2ndAttackTopRight
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.canWhiteQueenAttackTopRight or space.canWhiteQueen2ndAttackTopRight
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.canWhiteQueenAttackTopRight or space.canWhiteQueen2ndAttackTopRight
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.canWhiteQueenAttackTopRight or space.canWhiteQueen2ndAttackTopRight
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.canWhiteQueenAttackTopRight or space.canWhiteQueen2ndAttackTopRight
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.canWhiteQueenAttackTopRight or space.canWhiteQueen2ndAttackTopRight
    if(whoChess == "whitequeen" and kingWayDirection  == "bottomleft"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.canWhiteQueenAttackBottomLeft
    if(whoStuck == "whitequeen" and stuckWayDirection == "bottomleft"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.canWhiteQueenAttackBottomLeft or space.canWhiteQueen2ndAttackBottomLeft
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.canWhiteQueenAttackBottomLeft or space.canWhiteQueen2ndAttackBottomLeft
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.canWhiteQueenAttackBottomLeft or space.canWhiteQueen2ndAttackBottomLeft
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.canWhiteQueenAttackBottomLeft or space.canWhiteQueen2ndAttackBottomLeft
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.canWhiteQueenAttackBottomLeft or space.canWhiteQueen2ndAttackBottomLeft
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.canWhiteQueenAttackBottomLeft or space.canWhiteQueen2ndAttackBottomLeft
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.canWhiteQueenAttackBottomLeft or space.canWhiteQueen2ndAttackBottomLeft
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.canWhiteQueenAttackBottomLeft or space.canWhiteQueen2ndAttackBottomLeft
    if(whoChess == "whitequeen" and kingWayDirection  == "bottomright"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.canWhiteQueenAttackBottomRight
    if(whoStuck == "whitequeen" and stuckWayDirection == "bottomright"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.canWhiteQueenAttackBottomRight or space.canWhiteQueen2ndAttackBottomRight
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.canWhiteQueenAttackBottomRight or space.canWhiteQueen2ndAttackBottomRight
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.canWhiteQueenAttackBottomRight or space.canWhiteQueen2ndAttackBottomRight
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.canWhiteQueenAttackBottomRight or space.canWhiteQueen2ndAttackBottomRight
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.canWhiteQueenAttackBottomRight or space.canWhiteQueen2ndAttackBottomRight
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.canWhiteQueenAttackBottomRight or space.canWhiteQueen2ndAttackBottomRight
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.canWhiteQueenAttackBottomRight or space.canWhiteQueen2ndAttackBottomRight
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.canWhiteQueenAttackBottomRight or space.canWhiteQueen2ndAttackBottomRight
    if(whoChess == "whitequeen" and kingWayDirection  == "topleft"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.canWhiteQueenAttackTopLeft
    if(whoStuck == "whitequeen" and stuckWayDirection == "topleft"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.canWhiteQueenAttackTopLeft or space.canWhiteQueen2ndAttackTopLeft
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.canWhiteQueenAttackTopLeft or space.canWhiteQueen2ndAttackTopLeft
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.canWhiteQueenAttackTopLeft or space.canWhiteQueen2ndAttackTopLeft
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.canWhiteQueenAttackTopLeft or space.canWhiteQueen2ndAttackTopLeft
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.canWhiteQueenAttackTopLeft or space.canWhiteQueen2ndAttackTopLeft
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.canWhiteQueenAttackTopLeft or space.canWhiteQueen2ndAttackTopLeft
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.canWhiteQueenAttackTopLeft or space.canWhiteQueen2ndAttackTopLeft
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.canWhiteQueenAttackTopLeft or space.canWhiteQueen2ndAttackTopLeft
    if(whoChess == "blackqueen" and kingWayDirection  == "topright"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.canBlackQueenAttackTopTopRight
    if(whoStuck == "blackqueen" and stuckWayDirection == "topright"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.canBlackQueenAttackTopTopRight or space.canBlackQueen2ndAttackTopTopRight
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.canBlackQueenAttackTopTopRight or space.canBlackQueen2ndAttackTopTopRight
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.canBlackQueenAttackTopTopRight or space.canBlackQueen2ndAttackTopTopRight
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.canBlackQueenAttackTopTopRight or space.canBlackQueen2ndAttackTopTopRight
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.canBlackQueenAttackTopTopRight or space.canBlackQueen2ndAttackTopTopRight
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.canBlackQueenAttackTopTopRight or space.canBlackQueen2ndAttackTopTopRight
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.canBlackQueenAttackTopTopRight or space.canBlackQueen2ndAttackTopTopRight
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.canBlackQueenAttackTopTopRight or space.canBlackQueen2ndAttackTopTopRight
    if(whoChess == "blackqueen" and kingWayDirection  == "bottomleft"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.canBlackQueenAttackBottomLeft
    if(whoStuck == "blackqueen" and stuckWayDirection == "bottomleft"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.canBlackQueenAttackBottomLeft or space.canBlackQueen2ndAttackBottomLeft
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.canBlackQueenAttackBottomLeft or space.canBlackQueen2ndAttackBottomLeft
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.canBlackQueenAttackBottomLeft or space.canBlackQueen2ndAttackBottomLeft
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.canBlackQueenAttackBottomLeft or space.canBlackQueen2ndAttackBottomLeft
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.canBlackQueenAttackBottomLeft or space.canBlackQueen2ndAttackBottomLeft
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.canBlackQueenAttackBottomLeft or space.canBlackQueen2ndAttackBottomLeft
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.canBlackQueenAttackBottomLeft or space.canBlackQueen2ndAttackBottomLeft
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.canBlackQueenAttackBottomLeft or space.canBlackQueen2ndAttackBottomLeft
    if(whoChess == "blackqueen" and kingWayDirection  == "bottomright"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.canBlackQueenAttackBottomRight
    if(whoStuck == "blackqueen" and stuckWayDirection == "bottomright"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.canBlackQueenAttackBottomRight or space.canBlackQueen2ndAttackBottomRight
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.canBlackQueenAttackBottomRight or space.canBlackQueen2ndAttackBottomRight
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.canBlackQueenAttackBottomRight or space.canBlackQueen2ndAttackBottomRight
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.canBlackQueenAttackBottomRight or space.canBlackQueen2ndAttackBottomRight
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.canBlackQueenAttackBottomRight or space.canBlackQueen2ndAttackBottomRight
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.canBlackQueenAttackBottomRight or space.canBlackQueen2ndAttackBottomRight
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.canBlackQueenAttackBottomRight or space.canBlackQueen2ndAttackBottomRight
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.canBlackQueenAttackBottomRight or space.canBlackQueen2ndAttackBottomRight
    if(whoChess == "blackqueen" and kingWayDirection  == "topleft"):
        for space in spaces:
            space.wayOfKing = space.wayOfKing or space.canBlackQueenAttackTopLeft
    if(whoStuck == "blackqueen" and stuckWayDirection == "topleft"):
        for space in spaces:
            if ("rook1" in whoStucked):
                space.wayOfStuck1Rook = space.canBlackQueenAttackTopLeft or space.canBlackQueen2ndAttackTopLeft
            elif ("rook2" in whoStucked):
                space.wayOfStuck2Rook = space.canBlackQueenAttackTopLeft or space.canBlackQueen2ndAttackTopLeft
            elif ("knight1" in whoStucked):
                space.wayOfStuck1Knight = space.canBlackQueenAttackTopLeft or space.canBlackQueen2ndAttackTopLeft
            elif ("knight2" in whoStucked):
                space.wayOfStuck2Knight = space.canBlackQueenAttackTopLeft or space.canBlackQueen2ndAttackTopLeft
            elif ("bishop1" in whoStucked):
                space.wayOfStuck1Bishop = space.canBlackQueenAttackTopLeft or space.canBlackQueen2ndAttackTopLeft
            elif ("bishop2" in whoStucked):
                space.wayOfStuck2Bishop = space.canBlackQueenAttackTopLeft or space.canBlackQueen2ndAttackTopLeft
            elif ("queen" in whoStucked):
                space.wayOfStuckQueen = space.canBlackQueenAttackTopLeft or space.canBlackQueen2ndAttackTopLeft
            elif ("pawn" in whoStucked):
                space.wayOfStuck = space.canBlackQueenAttackTopLeft or space.canBlackQueen2ndAttackTopLeft
def selectAttackAreas():
    global spaces
    global chess
    global whoStuck
    global whoStucked
    global whoChess
    global kingWayDirection
    global stuckWayDirection
    chess = False
    for space in spaces:
        space.stuck = False

        space.canWhiteAttack = False
        space.canBlackAttack = False

        space.can1WhiteKnightAttack = False
        space.can2WhiteKnightAttack = False
        space.can1BlackKnightAttack = False
        space.can2BlackKnightAttack = False

        space.can1WhiteRookAttackTop = False
        space.can1WhiteRookAttackBottom = False
        space.can1WhiteRookAttackRight = False
        space.can1WhiteRookAttackLeft = False

        space.can1WhiteRook2ndAttackTop = False
        space.can1WhiteRook2ndAttackBottom = False
        space.can1WhiteRook2ndAttackRight = False
        space.can1WhiteRook2ndAttackLeft = False

        space.can2WhiteRookAttackTop = False
        space.can2WhiteRookAttackBottom = False
        space.can2WhiteRookAttackRight = False
        space.can2WhiteRookAttackLeft = False

        space.can2WhiteRook2ndAttackTop = False
        space.can2WhiteRook2ndAttackBottom = False
        space.can2WhiteRook2ndAttackRight = False
        space.can2WhiteRook2ndAttackLeft = False

        space.can1BlackRookAttackTop = False
        space.can1BlackRookAttackBottom = False
        space.can1BlackRookAttackRight = False
        space.can1BlackRookAttackLeft = False

        space.can1BlackRook2ndAttackTop = False
        space.can1BlackRook2ndAttackBottom = False
        space.can1BlackRook2ndAttackRight = False
        space.can1BlackRook2ndAttackLeft = False

        space.can2BlackRookAttackTop = False
        space.can2BlackRookAttackBottom = False
        space.can2BlackRookAttackRight = False
        space.can2BlackRookAttackLeft = False

        space.can2BlackRook2ndAttackTop = False
        space.can2BlackRook2ndAttackBottom = False
        space.can2BlackRook2ndAttackRight = False
        space.can2BlackRook2ndAttackLeft = False

        space.can1WhiteBishopAttackTop = False
        space.can1WhiteBishopAttackBottom = False
        space.can1WhiteBishopAttackRight = False
        space.can1WhiteBishopAttackLeft = False

        space.can1WhiteBishop2ndAttackTop = False
        space.can1WhiteBishop2ndAttackBottom = False
        space.can1WhiteBishop2ndAttackRight = False
        space.can1WhiteBishop2ndAttackLeft = False

        space.can2WhiteBishopAttackTop = False
        space.can2WhiteBishopAttackBottom = False
        space.can2WhiteBishopAttackRight = False
        space.can2WhiteBishopAttackLeft = False
        
        space.can2WhiteBishop2ndAttackTop = False
        space.can2WhiteBishop2ndAttackBottom = False
        space.can2WhiteBishop2ndAttackRight = False
        space.can2WhiteBishop2ndAttackLeft = False
        
        space.can1BlackBishopAttackTop = False
        space.can1BlackBishopAttackBottom = False
        space.can1BlackBishopAttackRight = False
        space.can1BlackBishopAttackLeft = False

        space.can1BlackBishop2ndAttackTop = False
        space.can1BlackBishop2ndAttackBottom = False
        space.can1BlackBishop2ndAttackRight = False
        space.can1BlackBishop2ndAttackLeft = False

        space.can2BlackBishopAttackTop = False
        space.can2BlackBishopAttackBottom = False
        space.can2BlackBishopAttackRight = False
        space.can2BlackBishopAttackLeft = False

        space.can2BlackBishop2ndAttackTop = False
        space.can2BlackBishop2ndAttackBottom = False
        space.can2BlackBishop2ndAttackRight = False
        space.can2BlackBishop2ndAttackLeft = False


        space.canWhiteQueenAttackTop = False
        space.canWhiteQueenAttackTopRight = False
        space.canWhiteQueenAttackRight = False
        space.canWhiteQueenAttackBottomRight = False
        space.canWhiteQueenAttackBottom = False
        space.canWhiteQueenAttackBottomLeft = False
        space.canWhiteQueenAttackLeft = False
        space.canWhiteQueenAttackTopLeft = False

        space.canWhiteQueen2ndAttackTop = False
        space.canWhiteQueen2ndAttackTopRight = False
        space.canWhiteQueen2ndAttackRight = False
        space.canWhiteQueen2ndAttackBottomRight = False
        space.canWhiteQueen2ndAttackBottom = False
        space.canWhiteQueen2ndAttackBottomLeft = False
        space.canWhiteQueen2ndAttackLeft = False
        space.canWhiteQueen2ndAttackTopLeft = False

        space.canBlackQueenAttackTop = False
        space.canBlackQueenAttackTopRight = False
        space.canBlackQueenAttackRight = False
        space.canBlackQueenAttackBottomRight = False
        space.canBlackQueenAttackBottom = False
        space.canBlackQueenAttackBottomLeft = False
        space.canBlackQueenAttackLeft = False
        space.canBlackQueenAttackTopLeft = False

        space.canBlackQueen2ndAttackTop = False
        space.canBlackQueen2ndAttackTopRight = False
        space.canBlackQueen2ndAttackRight = False
        space.canBlackQueen2ndAttackBottomRight = False
        space.canBlackQueen2ndAttackBottom = False
        space.canBlackQueen2ndAttackBottomLeft = False
        space.canBlackQueen2ndAttackLeft = False
        space.canBlackQueen2ndAttackTopLeft = False
    for space in spaces: 
        if ("whitepawn" in space.get_image().name):
            c = "{}{}"
            c = c.format(chr(ord(space.name[0]) + 1), chr(ord(space.name[1]) + 1))
            d = "{}{}"
            d = d.format(chr(ord(space.name[0]) - 1), chr(ord(space.name[1]) + 1))
            for spacex in spaces:
                if (spacex.name == c or spacex.name == d):
                    spacex.canWhiteAttack = True
                    if spacex.get_image().name == "blackking":
                        chess = True
                        whoChess = space.get_image().name
        elif ("blackpawn" in space.get_image().name):
            c = "{}{}"
            c = c.format(chr(ord(space.name[0]) + 1), chr(ord(space.name[1]) - 1))
            d = "{}{}"
            d = d.format(chr(ord(space.name[0]) - 1), chr(ord(space.name[1]) - 1))
            for spacex in spaces:
                if (spacex.name == c or spacex.name == d):
                    spacex.canBlackAttack = True
                    if spacex.get_image().name == "whiteking":
                        chess = True
        elif ("rook" in space.get_image().name):
            if("whiterook1" in space.get_image().name):
                space.can1WhiteRookAttackTop = True
                space.can1WhiteRookAttackBottom = True
                space.can1WhiteRookAttackRight = True
                space.can1WhiteRookAttackLeft = True
            elif("whiterook2" in space.get_image().name):
                space.can2WhiteRookAttackTop = True
                space.can2WhiteRookAttackBottom = True
                space.can2WhiteRookAttackRight = True 
                space.can2WhiteRookAttackLeft = True
            elif("blackrook1" in space.get_image().name):
                space.can1BlackRookAttackTop = True
                space.can1BlackRookAttackBottom = True
                space.can1BlackRookAttackRight = True
                space.can1BlackRookAttackLeft = True
            elif("blackrook2" in space.get_image().name):
                space.can2BlackRookAttackTop = True
                space.can2BlackRookAttackBottom = True
                space.can2BlackRookAttackRight = True
                space.can2BlackRookAttackLeft = True
            stuckName = ""
            y = 8
            z = 0
            for i in range(8):
                for spacex in spaces:
                    ch = "{}{}"
                    ch = ch.format(space.name[0] , chr(ord(space.name[1]) + i + 1))
                    if (spacex.name == ch and z < 2 ):
                        if (spacex.isAvaible == False and z == 0):
                            if("whiterook1" in space.get_image().name):
                                spacex.can1WhiteRookAttackTop = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "top"
                            elif("whiterook2" in space.get_image().name):
                                spacex.can2WhiteRookAttackTop = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "top"
                            elif("blackrook1" in space.get_image().name):
                                spacex.can1BlackRookAttackTop = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "top"
                            elif("blackrook2" in space.get_image().name):
                                spacex.can2BlackRookAttackTop = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "top"
                            y = i
                            z = 1
                            stuckName = spacex.name
                            break
                        elif (spacex.isAvaible == False and z == 1 ):
                            if (("white" in space.get_image().name and "blackking" in spacex.get_image().name) or ("black" in space.get_image().name and "whiteking" in spacex.get_image().name)):
                                for spacey in spaces:
                                    if (spacey.name == stuckName):
                                        spacey.stuck = True
                                        stuckWayDirection = "top"
                                        whoStucked = spacey.get_image().name
                                whoStuck = space.get_image().name
                            if("whiterook1" in space.get_image().name):
                                spacex.can1WhiteRook2ndAttackTop = True
                            elif("whiterook2" in space.get_image().name):
                                spacex.can2WhiteRook2ndAttackTop = True
                            elif("blackrook1" in space.get_image().name):
                                spacex.can1BlackRook2ndAttackTop = True
                            elif("blackrook2" in space.get_image().name):
                                spacex.can2BlackRook2ndAttackTop = True
                            z = 2
                            break
                        elif (z == 0):
                            if("whiterook1" in space.get_image().name):
                                spacex.can1WhiteRookAttackTop = True
                            elif("whiterook2" in space.get_image().name):
                                spacex.can2WhiteRookAttackTop = True
                            elif("blackrook1" in space.get_image().name):
                                spacex.can1BlackRookAttackTop = True
                            elif("blackrook2" in space.get_image().name):
                                spacex.can2BlackRookAttackTop = True
                        elif (z == 1):
                            if("whiterook1" in space.get_image().name):
                                spacex.can1WhiteRook2ndAttackTop = True
                            elif("whiterook2" in space.get_image().name):
                                spacex.can2WhiteRook2ndAttackTop = True
                            elif("blackrook1" in space.get_image().name):
                                spacex.can1BlackRook2ndAttackTop = True
                            elif("blackrook2" in space.get_image().name):
                                spacex.can2BlackRook2ndAttackTop = True
                if (i == y and z == 2):
                    break
            y = 8
            z = 0
            for i in range(8):
                for spacex in spaces:
                    ch = "{}{}"
                    ch = ch.format(space.name[0] , chr(ord(space.name[1]) - (i + 1)))
                    if (spacex.name == ch and z < 2 ):
                        if (spacex.isAvaible == False and z == 0):
                            if("whiterook1" in space.get_image().name):
                                spacex.can1WhiteRookAttackBottom = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "bottom"
                            elif("whiterook2" in space.get_image().name):
                                spacex.can2WhiteRookAttackBottom = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "bottom"
                            elif("blackrook1" in space.get_image().name):
                                spacex.can1BlackRookAttackBottom = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "bottom"
                            elif("blackrook2" in space.get_image().name):
                                spacex.can2BlackRookAttackBottom = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "bottom"
                            y = i
                            z = 1
                            stuckName = spacex.name
                            break
                        elif (spacex.isAvaible == False and z == 1 ):
                            if (("white" in space.get_image().name and "blackking" in spacex.get_image().name) or ("black" in space.get_image().name and "whiteking" in spacex.get_image().name)):
                                for spacey in spaces:
                                    if (spacey.name == stuckName):
                                        spacey.stuck = True
                                        stuckWayDirection = "bottom"
                                        whoStucked = spacey.get_image().name
                                whoStuck = space.get_image().name
                            if("whiterook1" in space.get_image().name):
                                spacex.can1WhiteRook2ndAttackBottom = True
                            elif("whiterook2" in space.get_image().name):
                                spacex.can2WhiteRook2ndAttackBottom = True
                            elif("blackrook1" in space.get_image().name):
                                spacex.can1BlackRook2ndAttackBottom = True
                            elif("blackrook2" in space.get_image().name):
                                spacex.can2BlackRook2ndAttackBottom = True
                            z = 2
                            break
                        elif (z == 0):
                            if("whiterook1" in space.get_image().name):
                                spacex.can1WhiteRookAttackBottom = True
                            elif("whiterook2" in space.get_image().name):
                                spacex.can2WhiteRookAttackBottom = True
                            elif("blackrook1" in space.get_image().name):
                                spacex.can1BlackRookAttackBottom = True
                            elif("blackrook2" in space.get_image().name):
                                spacex.can2BlackRookAttackBottom = True
                        elif (z == 1):
                            if("whiterook1" in space.get_image().name):
                                spacex.can1WhiteRook2ndAttackBottom = True
                            elif("whiterook2" in space.get_image().name):
                                spacex.can2WhiteRook2ndAttackBottom = True
                            elif("blackrook1" in space.get_image().name):
                                spacex.can1BlackRook2ndAttackBottom = True
                            elif("blackrook2" in space.get_image().name):
                                spacex.can2BlackRook2ndAttackBottom = True
                if (i == y and z == 2):
                    break
            y = 8
            z = 0
            for i in range(8):
                for spacex in spaces:
                    ch = "{}{}"
                    ch = ch.format(chr(ord(space.name[0]) + i + 1) , space.name[1] )
                    if (spacex.name == ch and z < 2 ):
                        if (spacex.isAvaible == False and z == 0):
                            if("whiterook1" in space.get_image().name):
                                spacex.can1WhiteRookAttackRight = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "right"
                            elif("whiterook2" in space.get_image().name):
                                spacex.can2WhiteRookAttackRight = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "right"
                            elif("blackrook1" in space.get_image().name):
                                spacex.can1BlackRookAttackRight = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "right"
                            elif("blackrook2" in space.get_image().name):
                                spacex.can2BlackRookAttackRight = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "right"
                            y = i
                            z = 1
                            stuckName = spacex.name
                            break
                        elif (spacex.isAvaible == False and z == 1 ):
                            if (("white" in space.get_image().name and "blackking" in spacex.get_image().name) or ("black" in space.get_image().name and "whiteking" in spacex.get_image().name)):
                                for spacey in spaces:
                                    if (spacey.name == stuckName):
                                        spacey.stuck = True
                                        stuckWayDirection = "right"
                                        whoStucked = spacey.get_image().name
                                whoStuck = space.get_image().name
                            if("whiterook1" in space.get_image().name):
                                spacex.can1WhiteRook2ndAttackRight = True
                            elif("whiterook2" in space.get_image().name):
                                spacex.can2WhiteRook2ndAttackRight = True
                            elif("blackrook1" in space.get_image().name):
                                spacex.can1BlackRook2ndAttackRight = True
                            elif("blackrook2" in space.get_image().name):
                                spacex.can2BlackRook2ndAttackRight = True
                            z = 2
                            break
                        elif (z == 0):
                            if("whiterook1" in space.get_image().name):
                                spacex.can1WhiteRookAttackRight = True
                            elif("whiterook2" in space.get_image().name):
                                spacex.can2WhiteRookAttackRight = True
                            elif("blackrook1" in space.get_image().name):
                                spacex.can1BlackRookAttackRight = True
                            elif("blackrook2" in space.get_image().name):
                                spacex.can2BlackRookAttackRight = True
                        elif (z == 1):
                            if("whiterook1" in space.get_image().name):
                                spacex.can1WhiteRook2ndAttackRight = True
                            elif("whiterook2" in space.get_image().name):
                                spacex.can2WhiteRook2ndAttackRight = True
                            elif("blackrook1" in space.get_image().name):
                                spacex.can1BlackRook2ndAttackRight = True
                            elif("blackrook2" in space.get_image().name):
                                spacex.can2BlackRook2ndAttackRight = True
                if (i == y and z == 2):
                    break
            y = 8
            z = 0
            for i in range(8):
                for spacex in spaces:
                    ch = "{}{}"
                    ch = ch.format(chr(ord(space.name[0]) - i - 1) , space.name[1] )
                    if (spacex.name == ch and z < 2 ):
                        if (spacex.isAvaible == False and z == 0):
                            if("whiterook1" in space.get_image().name):
                                spacex.can1WhiteRookAttackLeft = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "left"
                            elif("whiterook2" in space.get_image().name):
                                spacex.can2WhiteRookAttackLeft = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "left"
                            elif("blackrook1" in space.get_image().name):
                                spacex.can1BlackRookAttackLeft = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "left"
                            elif("blackrook2" in space.get_image().name):
                                spacex.can2BlackRookAttackLeft = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "left"
                            y = i
                            z = 1
                            stuckName = spacex.name
                            break
                        elif (spacex.isAvaible == False and z == 1 ):
                            if (("white" in space.get_image().name and "blackking" in spacex.get_image().name) or ("black" in space.get_image().name and "whiteking" in spacex.get_image().name)):
                                for spacey in spaces:
                                    if (spacey.name == stuckName):
                                        spacey.stuck = True
                                        stuckWayDirection = "left"
                                        whoStucked = spacey.get_image().name
                                whoStuck = space.get_image().name
                            if("whiterook1" in space.get_image().name):
                                spacex.can1WhiteRook2ndAttackLeft = True
                            elif("whiterook2" in space.get_image().name):
                                spacex.can2WhiteRook2ndAttackLeft = True
                            elif("blackrook1" in space.get_image().name):
                                spacex.can1BlackRook2ndAttackLeft = True
                            elif("blackrook2" in space.get_image().name):
                                spacex.can2BlackRook2ndAttackLeft = True
                            z = 2
                            break
                        elif (z == 0):
                            if("whiterook1" in space.get_image().name):
                                spacex.can1WhiteRookAttackLeft = True
                            elif("whiterook2" in space.get_image().name):
                                spacex.can2WhiteRookAttackLeft = True
                            elif("blackrook1" in space.get_image().name):
                                spacex.can1BlackRookAttackLeft = True
                            elif("blackrook2" in space.get_image().name):
                                spacex.can2BlackRookAttackLeft = True
                        elif (z == 1):
                            if("whiterook1" in space.get_image().name):
                                spacex.can1WhiteRook2ndAttackLeft = True
                            elif("whiterook2" in space.get_image().name):
                                spacex.can2WhiteRook2ndAttackLeft = True
                            elif("blackrook1" in space.get_image().name):
                                spacex.can1BlackRook2ndAttackLeft = True
                            elif("blackrook2" in space.get_image().name):
                                spacex.can2BlackRook2ndAttackLeft = True
                if (i == y and z == 2):
                    break  
        elif ("bishop" in space.get_image().name):
            if("whitebishop1" in space.get_image().name):
                space.can1WhiteBishopAttackTop = True
                space.can1WhiteBishopAttackBottom = True
                space.can1WhiteBishopAttackRight = True
                space.can1WhiteBishopAttackLeft = True
            elif("whitebishop2" in space.get_image().name):
                space.can2WhiteBishopAttackTop = True
                space.can2WhiteBishopAttackBottom = True
                space.can2WhiteBishopAttackRight = True 
                space.can2WhiteBishopAttackLeft = True
            elif("blackbishop1" in space.get_image().name):
                space.can1BlackBishopAttackTop = True
                space.can1BlackBishopAttackBottom = True
                space.can1BlackBishopAttackRight = True
                space.can1BlackBishopAttackLeft = True
            elif("blackbishop2" in space.get_image().name):
                space.can2BlackBishopAttackTop = True
                space.can2BlackBishopAttackBottom = True
                space.can2BlackBishopAttackRight = True
                space.can2BlackBishopAttackLeft = True
            stuckName = ""
            y = 8
            z = 0
            for i in range(8):
                for spacex in spaces:
                    ch = "{}{}"
                    ch = ch.format( chr(ord(space.name[0]) + i + 1) , chr(ord(space.name[1]) + i + 1))
                    if (spacex.name == ch and z < 2 ):
                        if (spacex.isAvaible == False and z == 0):
                            if("whitebishop1" in space.get_image().name):
                                spacex.can1WhiteBishopAttackTop = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "top"
                            elif("whitebishop2" in space.get_image().name):
                                spacex.can2WhiteBishopAttackTop = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "top"
                            elif("blackbishop1" in space.get_image().name):
                                spacex.can1BlackBishopAttackTop = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "top"
                            elif("blackbishop2" in space.get_image().name):
                                spacex.can2BlackBishopAttackTop = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "top"
                            y = i
                            z = 1
                            stuckName = spacex.name
                            break
                        elif (spacex.isAvaible == False and z == 1 ):
                            if (("white" in space.get_image().name and "blackking" in spacex.get_image().name) or ("black" in space.get_image().name and "whiteking" in spacex.get_image().name)):
                                for spacey in spaces:
                                    if (spacey.name == stuckName):
                                        spacey.stuck = True
                                        stuckWayDirection = "top"
                                        whoStucked = spacey.get_image().name
                                whoStuck = space.get_image().name
                            if("whitebishop1" in space.get_image().name):
                                spacex.can1WhiteBishop2ndAttackTop = True
                            elif("whitebishop2" in space.get_image().name):
                                spacex.can2WhiteBishop2ndAttackTop = True
                            elif("blackbishop1" in space.get_image().name):
                                spacex.can1BlackBishop2ndAttackTop = True
                            elif("blackbishop2" in space.get_image().name):
                                spacex.can2BlackBishop2ndAttackTop = True
                            z = 2
                            break
                        elif (z == 0):
                            if("whitebishop1" in space.get_image().name):
                                spacex.can1WhiteBishopAttackTop = True
                            elif("whitebishop2" in space.get_image().name):
                                spacex.can2WhiteBishopAttackTop = True
                            elif("blackbishop1" in space.get_image().name):
                                spacex.can1BlackBishopAttackTop = True
                            elif("blackbishop2" in space.get_image().name):
                                spacex.can2BlackBishopAttackTop = True
                        elif (z == 1):
                            if("whitebishop1" in space.get_image().name):
                                spacex.can1WhiteBishop2ndAttackTop = True
                            elif("whitebishop2" in space.get_image().name):
                                spacex.can2WhiteBishop2ndAttackTop = True
                            elif("blackbishop1" in space.get_image().name):
                                spacex.can1BlackBishop2ndAttackTop = True
                            elif("blackbishop2" in space.get_image().name):
                                spacex.can2BlackBishop2ndAttackTop = True
                if (i == y and z == 2):
                    break
            y = 8
            z = 0
            for i in range(8):
                for spacex in spaces:
                    ch = "{}{}"
                    ch = ch.format(chr(ord(space.name[0]) - (i + 1)) , chr(ord(space.name[1]) - (i + 1)))
                    if (spacex.name == ch and z < 2 ):
                        if (spacex.isAvaible == False and z == 0):
                            if("whitebishop1" in space.get_image().name):
                                spacex.can1WhiteBishopAttackBottom = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "bottom"
                            elif("whitebishop2" in space.get_image().name):
                                spacex.can2WhiteBishopAttackBottom = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "bottom"
                            elif("blackbishop1" in space.get_image().name):
                                spacex.can1BlackBishopAttackBottom = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "bottom"
                            elif("blackbishop2" in space.get_image().name):
                                spacex.can2BlackBishopAttackBottom = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "bottom"
                            y = i
                            z = 1
                            stuckName = spacex.name
                            break
                        elif (spacex.isAvaible == False and z == 1 ):
                            if (("white" in space.get_image().name and "blackking" in spacex.get_image().name) or ("black" in space.get_image().name and "whiteking" in spacex.get_image().name)):
                                for spacey in spaces:
                                    if (spacey.name == stuckName):
                                        spacey.stuck = True
                                        stuckWayDirection = "bottom"
                                        whoStucked = spacey.get_image().name
                                whoStuck = space.get_image().name
                            if("whitebishop1" in space.get_image().name):
                                spacex.can1WhiteBishop2ndAttackBottom = True
                            elif("whitebishop2" in space.get_image().name):
                                spacex.can2WhiteBishop2ndAttackBottom = True
                            elif("blackbishop1" in space.get_image().name):
                                spacex.can1BlackBishop2ndAttackBottom = True
                            elif("blackbishop2" in space.get_image().name):
                                spacex.can2BlackBishop2ndAttackBottom = True
                            z = 2
                            break
                        elif (z == 0):
                            if("whitebishop1" in space.get_image().name):
                                spacex.can1WhiteBishopAttackBottom = True
                            elif("whitebishop2" in space.get_image().name):
                                spacex.can2WhiteBishopAttackBottom = True
                            elif("blackbishop1" in space.get_image().name):
                                spacex.can1BlackBishopAttackBottom = True
                            elif("blackbishop2" in space.get_image().name):
                                spacex.can2BlackBishopAttackBottom = True
                        elif (z == 1):
                            if("whitebishop1" in space.get_image().name):
                                spacex.can1WhiteBishop2ndAttackBottom = True
                            elif("whitebishop2" in space.get_image().name):
                                spacex.can2WhiteBishop2ndAttackBottom = True
                            elif("blackbishop1" in space.get_image().name):
                                spacex.can1BlackBishop2ndAttackBottom = True
                            elif("blackbishop2" in space.get_image().name):
                                spacex.can2BlackBishop2ndAttackBottom = True
                if (i == y and z == 2):
                    break
            y = 8
            z = 0
            for i in range(8):
                for spacex in spaces:
                    ch = "{}{}"
                    ch = ch.format(chr(ord(space.name[0]) + i + 1) , chr(ord(space.name[1]) - i - 1) )
                    if (spacex.name == ch and z < 2 ):
                        if (spacex.isAvaible == False and z == 0):
                            if("whitebishop1" in space.get_image().name):
                                spacex.can1WhiteBishopAttackRight = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "right"
                            elif("whitebishop2" in space.get_image().name):
                                spacex.can2WhiteBishopAttackRight = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "right"
                            elif("blackbishop1" in space.get_image().name):
                                spacex.can1BlackBishopAttackRight = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "right"
                            elif("blackbishop2" in space.get_image().name):
                                spacex.can2BlackBishopAttackRight = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "right"
                            y = i
                            z = 1
                            stuckName = spacex.name
                            break
                        elif (spacex.isAvaible == False and z == 1 ):
                            if (("white" in space.get_image().name and "blackking" in spacex.get_image().name) or ("black" in space.get_image().name and "whiteking" in spacex.get_image().name)):
                                for spacey in spaces:
                                    if (spacey.name == stuckName):
                                        spacey.stuck = True
                                        stuckWayDirection = "right"
                                        whoStucked = spacey.get_image().name
                                whoStuck = space.get_image().name
                            if("whitebishop1" in space.get_image().name):
                                spacex.can1WhiteBishop2ndAttackRight = True
                            elif("whitebishop2" in space.get_image().name):
                                spacex.can2WhiteBishop2ndAttackRight = True
                            elif("blackbishop1" in space.get_image().name):
                                spacex.can1BlackBishop2ndAttackRight = True
                            elif("blackbishop2" in space.get_image().name):
                                spacex.can2BlackBishop2ndAttackRight = True
                            z = 2
                            break
                        elif (z == 0):
                            if("whitebishop1" in space.get_image().name):
                                spacex.can1WhiteBishopAttackRight = True
                            elif("whitebishop2" in space.get_image().name):
                                spacex.can2WhiteBishopAttackRight = True
                            elif("blackbishop1" in space.get_image().name):
                                spacex.can1BlackBishopAttackRight = True
                            elif("blackbishop2" in space.get_image().name):
                                spacex.can2BlackBishopAttackRight = True
                        elif (z == 1):
                            if("whitebishop1" in space.get_image().name):
                                spacex.can1WhiteBishop2ndAttackRight = True
                            elif("whitebishop2" in space.get_image().name):
                                spacex.can2WhiteBishop2ndAttackRight = True
                            elif("blackbishop1" in space.get_image().name):
                                spacex.can1BlackBishop2ndAttackRight = True
                            elif("blackbishop2" in space.get_image().name):
                                spacex.can2BlackBishop2ndAttackRight = True
                if (i == y and z == 2):
                    break
            y = 8
            z = 0
            for i in range(8):
                for spacex in spaces:
                    ch = "{}{}"
                    ch = ch.format(chr(ord(space.name[0]) - i - 1) , chr(ord(space.name[1]) + i + 1) )
                    if (spacex.name == ch and z < 2 ):
                        if (spacex.isAvaible == False and z == 0):
                            if("whitebishop1" in space.get_image().name):
                                spacex.can1WhiteBishopAttackLeft = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "left"
                            elif("whitebishop2" in space.get_image().name):
                                spacex.can2WhiteBishopAttackLeft = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "left"
                            elif("blackbishop1" in space.get_image().name):
                                spacex.can1BlackBishopAttackLeft = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "left"
                            elif("blackbishop2" in space.get_image().name):
                                spacex.can2BlackBishopAttackLeft = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "left"
                            y = i
                            z = 1
                            stuckName = spacex.name
                            break
                        elif (spacex.isAvaible == False and z == 1 ):
                            if (("white" in space.get_image().name and "blackking" in spacex.get_image().name) or ("black" in space.get_image().name and "whiteking" in spacex.get_image().name)):
                                for spacey in spaces:
                                    if (spacey.name == stuckName):
                                        spacey.stuck = True
                                        stuckWayDirection = "left"
                                        whoStucked = spacey.get_image().name
                                whoStuck = space.get_image().name
                            if("whitebishop1" in space.get_image().name):
                                spacex.can1WhiteBishop2ndAttackLeft = True
                            elif("whitebishop2" in space.get_image().name):
                                spacex.can2WhiteBishop2ndAttackLeft = True
                            elif("blackbishop1" in space.get_image().name):
                                spacex.can1BlackBishop2ndAttackLeft = True
                            elif("blackbishop2" in space.get_image().name):
                                spacex.can2BlackBishop2ndAttackLeft = True
                            z = 2
                            break
                        elif (z == 0):
                            if("whitebishop1" in space.get_image().name):
                                spacex.can1WhiteBishopAttackLeft = True
                            elif("whitebishop2" in space.get_image().name):
                                spacex.can2WhiteBishopAttackLeft = True
                            elif("blackbishop1" in space.get_image().name):
                                spacex.can1BlackBishopAttackLeft = True
                            elif("blackbishop2" in space.get_image().name):
                                spacex.can2BlackBishopAttackLeft = True
                        elif (z == 1):
                            if("whitebishop1" in space.get_image().name):
                                spacex.can1WhiteBishop2ndAttackLeft = True
                            elif("whitebishop2" in space.get_image().name):
                                spacex.can2WhiteBishop2ndAttackLeft = True
                            elif("blackbishop1" in space.get_image().name):
                                spacex.can1BlackBishop2ndAttackLeft = True
                            elif("blackbishop2" in space.get_image().name):
                                spacex.can2BlackBishop2ndAttackLeft = True
                if (i == y and z == 2):
                    break  
        elif ("queen" in space.get_image().name):
            if("whitequeen" in space.get_image().name):
                space.canWhiteQueenAttackTop = True
                space.canWhiteQueenAttackTopRight = True
                space.canWhiteQueenAttackRight = True
                space.canWhiteQueenAttackBottomRight = True
                space.canWhiteQueenAttackBottom = True
                space.canWhiteQueenAttackBottomLeft = True
                space.canWhiteQueenAttackLeft = True
                space.canWhiteQueenAttackTopLeft = True
            elif("blackqueen" in space.get_image().name):
                space.canBlackQueenAttackTop = True
                space.canBlackQueenAttackTopRight = True
                space.canBlackQueenAttackRight = True
                space.canBlackQueenAttackBottomRight = True
                space.canBlackQueenAttackBottom = True
                space.canBlackQueenAttackBottomLeft = True
                space.canBlackQueenAttackLeft = True
                space.canBlackQueenAttackTopLeft = True
            stuckName = ""
            y = 8
            z = 0
            for i in range(8):
                for spacex in spaces:
                    ch = "{}{}"
                    ch = ch.format(space.name[0] , chr(ord(space.name[1]) + i + 1))
                    if (spacex.name == ch and z < 2 ):
                        if (spacex.isAvaible == False and z == 0):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueenAttackTop = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "top"
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueenAttackTop = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "top"
                            y = i
                            z = 1
                            stuckName = spacex.name
                            break
                        elif (spacex.isAvaible == False and z == 1 ):
                            if (("white" in space.get_image().name and "blackking" in spacex.get_image().name) or ("black" in space.get_image().name and "whiteking" in spacex.get_image().name)):
                                for spacey in spaces:
                                    if (spacey.name == stuckName):
                                        spacey.stuck = True
                                        stuckWayDirection = "top"
                                        whoStucked = spacey.get_image().name
                                whoStuck = space.get_image().name
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueen2ndAttackTop = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueen2ndAttackTop = True
                            z = 2
                            break
                        elif (z == 0):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueenAttackTop = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueenAttackTop = True
                        elif (z == 1):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueen2ndAttackTop = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueen2ndAttackTop = True
                if (i == y and z == 2):
                    break
            y = 8
            z = 0
            for i in range(8):
                for spacex in spaces:
                    ch = "{}{}"
                    ch = ch.format(space.name[0] , chr(ord(space.name[1]) - (i + 1)))
                    if (spacex.name == ch and z < 2 ):
                        if (spacex.isAvaible == False and z == 0):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueenAttackBottom = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "bottom"
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueenAttackBottom = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "bottom"
                            y = i
                            z = 1
                            stuckName = spacex.name
                            break
                        elif (spacex.isAvaible == False and z == 1 ):
                            if (("white" in space.get_image().name and "blackking" in spacex.get_image().name) or ("black" in space.get_image().name and "whiteking" in spacex.get_image().name)):
                                for spacey in spaces:
                                    if (spacey.name == stuckName):
                                        spacey.stuck = True
                                        stuckWayDirection = "bottom"
                                        whoStucked = spacey.get_image().name
                                whoStuck = space.get_image().name
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueen2ndAttackBottom = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueen2ndAttackBottom = True
                            z = 2
                            break
                        elif (z == 0):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueenAttackBottom = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueenAttackBottom = True
                        elif (z == 1):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueen2ndAttackBottom = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueen2ndAttackBottom = True
                if (i == y and z == 2):
                    break
            y = 8
            z = 0
            for i in range(8):
                for spacex in spaces:
                    ch = "{}{}"
                    ch = ch.format(chr(ord(space.name[0]) + i + 1) , space.name[1] )
                    if (spacex.name == ch and z < 2 ):
                        if (spacex.isAvaible == False and z == 0):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueenAttackRight = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "right"
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueenAttackRight = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "right"
                            y = i
                            z = 1
                            stuckName = spacex.name
                            break
                        elif (spacex.isAvaible == False and z == 1 ):
                            if (("white" in space.get_image().name and "blackking" in spacex.get_image().name) or ("black" in space.get_image().name and "whiteking" in spacex.get_image().name)):
                                for spacey in spaces:
                                    if (spacey.name == stuckName):
                                        spacey.stuck = True
                                        stuckWayDirection = "right"
                                        whoStucked = spacey.get_image().name
                                whoStuck = space.get_image().name
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueen2ndAttackRight = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueen2ndAttackRight = True
                            z = 2
                            break
                        elif (z == 0):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueenAttackRight = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueenAttackRight = True
                        elif (z == 1):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueen2ndAttackRight = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueen2ndAttackRight = True
                if (i == y and z == 2):
                    break
            y = 8
            z = 0
            for i in range(8):
                for spacex in spaces:
                    ch = "{}{}"
                    ch = ch.format(chr(ord(space.name[0]) - i - 1) , space.name[1] )
                    if (spacex.name == ch and z < 2 ):
                        if (spacex.isAvaible == False and z == 0):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueenAttackLeft = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "left"
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueenAttackLeft = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "left"
                            y = i
                            z = 1
                            stuckName = spacex.name
                            break
                        elif (spacex.isAvaible == False and z == 1 ):
                            if (("white" in space.get_image().name and "blackking" in spacex.get_image().name) or ("black" in space.get_image().name and "whiteking" in spacex.get_image().name)):
                                for spacey in spaces:
                                    if (spacey.name == stuckName):
                                        spacey.stuck = True
                                        stuckWayDirection = "left"
                                        whoStucked = spacey.get_image().name
                                whoStuck = space.get_image().name
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueen2ndAttackLeft = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueen2ndAttackLeft = True
                            z = 2
                            break
                        elif (z == 0):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueenAttackLeft = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueenAttackLeft = True
                        elif (z == 1):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueen2ndAttackLeft = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueen2ndAttackLeft = True
                if (i == y and z == 2):
                    break
            y = 8
            z = 0
            for i in range(8):
                for spacex in spaces:
                    ch = "{}{}"
                    ch = ch.format(chr(ord(space.name[0]) + i + 1) ,chr(ord(space.name[1]) + i + 1) )
                    if (spacex.name == ch and z < 2 ):
                        if (spacex.isAvaible == False and z == 0):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueenAttackTopRight = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "topright"
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueenAttackTopRight = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "topright"
                            y = i
                            z = 1
                            stuckName = spacex.name
                            break
                        elif (spacex.isAvaible == False and z == 1 ):
                            if (("white" in space.get_image().name and "blackking" in spacex.get_image().name) or ("black" in space.get_image().name and "whiteking" in spacex.get_image().name)):
                                for spacey in spaces:
                                    if (spacey.name == stuckName):
                                        spacey.stuck = True
                                        stuckWayDirection = "left"
                                        whoStucked = spacey.get_image().name
                                whoStuck = space.get_image().name
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueen2ndAttackTopRight = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueen2ndAttackTopRight = True
                            z = 2
                            break
                        elif (z == 0):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueenAttackTopRight = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueenAttackTopRight = True
                        elif (z == 1):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueen2ndAttackTopRight = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueen2ndAttackTopRight = True
                if (i == y and z == 2):
                    break
            y = 8
            z = 0
            for i in range(8):
                for spacex in spaces:
                    ch = "{}{}"
                    ch = ch.format(chr(ord(space.name[0]) + i + 1) ,chr(ord(space.name[1]) - i - 1) )
                    if (spacex.name == ch and z < 2 ):
                        if (spacex.isAvaible == False and z == 0):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueenAttackBottomRight = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "bottomright"
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueenAttackBottomRight = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "bottomright"
                            y = i
                            z = 1
                            stuckName = spacex.name
                            break
                        elif (spacex.isAvaible == False and z == 1 ):
                            if (("white" in space.get_image().name and "blackking" in spacex.get_image().name) or ("black" in space.get_image().name and "whiteking" in spacex.get_image().name)):
                                for spacey in spaces:
                                    if (spacey.name == stuckName):
                                        spacey.stuck = True
                                        stuckWayDirection = "left"
                                        whoStucked = spacey.get_image().name
                                whoStuck = space.get_image().name
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueen2ndAttackBottomRight = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueen2ndAttackBottomRight = True
                            z = 2
                            break
                        elif (z == 0):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueenAttackBottomRight = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueenAttackBottomRight = True
                        elif (z == 1):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueen2ndAttackBottomRight = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueen2ndAttackBottomRight = True
                if (i == y and z == 2):
                    break
            y = 8
            z = 0
            for i in range(8):
                for spacex in spaces:
                    ch = "{}{}"
                    ch = ch.format(chr(ord(space.name[0]) - i - 1) ,chr(ord(space.name[1]) - i - 1) )
                    if (spacex.name == ch and z < 2 ):
                        if (spacex.isAvaible == False and z == 0):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueenAttackBottomLeft = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "bottomleft"
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueenAttackBottomLeft = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "bottomleft"
                            y = i
                            z = 1
                            stuckName = spacex.name
                            break
                        elif (spacex.isAvaible == False and z == 1 ):
                            if (("white" in space.get_image().name and "blackking" in spacex.get_image().name) or ("black" in space.get_image().name and "whiteking" in spacex.get_image().name)):
                                for spacey in spaces:
                                    if (spacey.name == stuckName):
                                        spacey.stuck = True
                                        stuckWayDirection = "left"
                                        whoStucked = spacey.get_image().name
                                whoStuck = space.get_image().name
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueen2ndAttackBottomLeft = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueen2ndAttackBottomLeft = True
                            z = 2
                            break
                        elif (z == 0):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueenAttackBottomLeft = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueenAttackBottomLeft = True
                        elif (z == 1):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueen2ndAttackBottomLeft = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueen2ndAttackBottomLeft = True
                if (i == y and z == 2):
                    break
            y = 8
            z = 0
            for i in range(8):
                for spacex in spaces:
                    ch = "{}{}"
                    ch = ch.format(chr(ord(space.name[0]) - i - 1) ,chr(ord(space.name[1]) + i + 1) )
                    if (spacex.name == ch and z < 2 ):
                        if (spacex.isAvaible == False and z == 0):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueenAttackTopLeft = True
                                if ("blackking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "topleft"
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueenAttackTopLeft = True
                                if ("whiteking" in spacex.get_image().name):
                                    chess = True
                                    whoChess = space.get_image().name
                                    kingWayDirection = "topleft"
                            y = i
                            z = 1
                            stuckName = spacex.name
                            break
                        elif (spacex.isAvaible == False and z == 1 ):
                            if (("white" in space.get_image().name and "blackking" in spacex.get_image().name) or ("black" in space.get_image().name and "whiteking" in spacex.get_image().name)):
                                for spacey in spaces:
                                    if (spacey.name == stuckName):
                                        spacey.stuck = True
                                        stuckWayDirection = "left"
                                        whoStucked = spacey.get_image().name
                                whoStuck = space.get_image().name
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueen2ndAttackTopLeft = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueen2ndAttackTopLeft = True
                            z = 2
                            break
                        elif (z == 0):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueenAttackTopLeft = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueenAttackTopLeft = True
                        elif (z == 1):
                            if("whitequeen" in space.get_image().name):
                                spacex.canWhiteQueen2ndAttackTopLeft = True
                            elif("blackqueen" in space.get_image().name):
                                spacex.canBlackQueen2ndAttackTopLeft = True
                if (i == y and z == 2):
                    break
        elif ("knight" in space.get_image().name):
            a = "{}{}"
            a = a.format( chr(ord(space.name[0]) + 2) , chr(ord(space.name[1]) + 1))
            b = "{}{}"
            b = b.format( chr(ord(space.name[0]) + 2) , chr(ord(space.name[1]) - 1))
            c = "{}{}"
            c = c.format( chr(ord(space.name[0]) - 2) , chr(ord(space.name[1]) + 1))
            d = "{}{}"
            d = d.format( chr(ord(space.name[0]) - 2) , chr(ord(space.name[1]) - 1))
            e = "{}{}"
            e = e.format( chr(ord(space.name[0]) + 1) , chr(ord(space.name[1]) + 2))
            f = "{}{}"
            f = f.format( chr(ord(space.name[0]) + 1) , chr(ord(space.name[1]) - 2))
            g = "{}{}"
            g = g.format( chr(ord(space.name[0]) - 1) , chr(ord(space.name[1]) + 2))
            h = "{}{}"
            h = h.format( chr(ord(space.name[0]) - 1) , chr(ord(space.name[1]) - 2))
            if("whiteknight1" in space.get_image().name):
                for spacex in spaces:
                    if (spacex.name == a or spacex.name == b or spacex.name == c or spacex.name == d or spacex.name == e or spacex.name == f or spacex.name == g or spacex.name == h):
                        spacex.can1WhiteKnightAttack = True
                        if spacex.get_image().name == "blackking":
                            chess = True
                            whoChess = space.get_image().name
            if("whiteknight2" in space.get_image().name):
                for spacex in spaces:
                    if (spacex.name == a or spacex.name == b or spacex.name == c or spacex.name == d or spacex.name == e or spacex.name == f or spacex.name == g or spacex.name == h):
                        spacex.can2WhiteKnightAttack = True
                        if spacex.get_image().name == "blackking":
                            chess = True
                            whoChess = spacex.name
            if("blackknight1" in space.get_image().name):
                for spacex in spaces:
                    if (spacex.name == a or spacex.name == b or spacex.name == c or spacex.name == d or spacex.name == e or spacex.name == f or spacex.name == g or spacex.name == h):
                        spacex.can1BlackKnightAttack = True
                        if spacex.get_image().name == "whiteking":
                            chess = True
                            whoChess = spacex.name
            if("blackknight2" in space.get_image().name):
                for spacex in spaces:
                    if (spacex.name == a or spacex.name == b or spacex.name == c or spacex.name == d or spacex.name == e or spacex.name == f or spacex.name == g or spacex.name == h):
                        spacex.can2BlackKnightAttack = True
                        if spacex.get_image().name == "whiteking":
                            chess = True
                            whoChess = spacex.name

            
    findKingWay()
    for space in spaces:
        if space.stuck:
            print(space.stuck)
            print("works")
            print(whoStuck)
            print(whoStucked)
        #if space.wayOfKing:
        #    space.override_background_color(0,colorSelected)
        #if space.canWhiteQueen2ndAttackTopLeft:
        #    space.override_background_color(0,color2nd)
        if(space.wayOfStuck2Bishop):
            print(space.name)
    if(chess):
        print("Chess")
def move (widget,mode):

    global whiteTurn
    global settedPiece
    global settedPieceName
    global setted2Piece
    global setted2PieceName
    global isWhiteKingMove
    global isBlackKingMove
    global isWhiteRookMove
    global isBlackRookMove
    global numberOfChess
    global spaces

    if(mode):
        setted2PieceName = widget.get_image().name
    if ("whitepawn" in settedPieceName):
        image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/whitepawn.png")
        image.name = "whitepawn"
        widget.set_image (image)
        widget.isAvaible = False
        whiteTurn = False
        for space in spaces:
            space.override_background_color(0,space.defaultColor)
            space.isSelected = False
            if (space.name == settedPiece and mode):
                image = Gtk.Image()
                image.name = "blank"
                space.set_image(image)
                space.isAvaible = True
            if (space.name == settedPiece and mode == False):
                image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/" + setted2PieceName + ".png")
                image.name =setted2PieceName
                space.set_image(image)
                space.isAvaible = False
                whiteTurn = True
    elif ("blackpawn" in settedPieceName):
        image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/blackpawn.png")
        image.name = "blackpawn"
        widget.set_image (image)
        widget.isAvaible = False
        whiteTurn = True
        for space in spaces:
            space.override_background_color(0,space.defaultColor)
            space.isSelected = False
            if (space.name == settedPiece and mode):
                image = Gtk.Image()
                image.name = "blank"
                space.set_image(image)
                space.isAvaible = True
            if (space.name == settedPiece and mode == False):
                image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/" + setted2PieceName + ".png")
                image.name =setted2PieceName
                space.set_image(image)
                space.isAvaible = False
                whiteTurn = False
    elif ("whiterook" in settedPieceName):
        image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/whiterook.png")
        if ("1" in settedPieceName):
            image.name = "whiterook1"
        else:
            image.name = "whiterook2"

        widget.set_image (image)
        widget.isAvaible = False
        whiteTurn = False
        if (settedPiece == "H1"):
            isWhiteRookMove = True
        for space in spaces:
            space.override_background_color(0,space.defaultColor)
            space.isSelected = False
            if (space.name == settedPiece):
                image = Gtk.Image()
                image.name = "blank"
                space.set_image(image)
                space.isAvaible = True
            if (space.name == settedPiece and mode == False):
                image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/" + setted2PieceName + ".png")
                image.name =setted2PieceName
                space.set_image(image)
                space.isAvaible = False
                whiteTurn = True
    elif ("blackrook" in settedPieceName):
        image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/blackrook.png")
        if ("1" in settedPieceName):
            image.name = "blackrook1"
        else:
            image.name = "blackrook2"

        widget.set_image (image)
        widget.isAvaible = False
        whiteTurn = True
        if (settedPiece == "H8"):
            isBlackRookMove = True
        for space in spaces:
            space.override_background_color(0,space.defaultColor)
            space.isSelected = False
            if (space.name == settedPiece):
                image = Gtk.Image()
                image.name = "blank"
                space.set_image(image)
                space.isAvaible = True
            if (space.name == settedPiece and mode == False):
                image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/" + setted2PieceName + ".png")
                image.name =setted2PieceName
                space.set_image(image)
                space.isAvaible = False
                whiteTurn = False
    elif ("whitebishop" in settedPieceName):
        image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/whitebishop.png")
        if("1" in settedPieceName):
            image.name = "whitebishop1"
        else:
            image.name = "whitebishop2"

        widget.set_image (image)
        widget.isAvaible = False
        whiteTurn = False
        for space in spaces:
            space.override_background_color(0,space.defaultColor)
            space.isSelected = False
            if (space.name == settedPiece):
                image = Gtk.Image()
                image.name = "blank"
                space.set_image(image)
                space.isAvaible = True
            if (space.name == settedPiece and mode == False):
                image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/" + setted2PieceName + ".png")
                image.name =setted2PieceName
                space.set_image(image)
                space.isAvaible = False
                whiteTurn = True
    elif ("blackbishop" in settedPieceName):
        image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/blackbishop.png")
        if ("1" in settedPieceName):
            image.name = "blackbishop1"
        else:
            image.name = "blackbishop2"

        widget.set_image (image)
        widget.isAvaible = False
        whiteTurn = True
        for space in spaces:
            space.override_background_color(0,space.defaultColor)
            space.isSelected = False
            if (space.name == settedPiece):
                image = Gtk.Image()
                image.name = "blank"
                space.set_image(image)
                space.isAvaible = True
            if (space.name == settedPiece and mode == False):
                image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/" + setted2PieceName + ".png")
                image.name =setted2PieceName
                space.set_image(image)
                space.isAvaible = False
                whiteTurn = False
    elif ("whitequeen" in settedPieceName):
        image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/whitequeen.png")
        image.name = "whitequeen"

        widget.set_image (image)
        widget.isAvaible = False
        whiteTurn = False
        for space in spaces:
            space.override_background_color(0,space.defaultColor)
            space.isSelected = False
            if (space.name == settedPiece):
                image = Gtk.Image()
                image.name = "blank"
                space.set_image(image)
                space.isAvaible = True
            if (space.name == settedPiece and mode == False):
                image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/" + setted2PieceName + ".png")
                image.name =setted2PieceName
                space.set_image(image)
                space.isAvaible = False
                whiteTurn = True
    elif ("blackqueen" in settedPieceName):
        image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/blackqueen.png")
        image.name = "blackqueen"

        widget.set_image (image)
        widget.isAvaible = False
        whiteTurn = True
        for space in spaces:
            space.override_background_color(0,space.defaultColor)
            space.isSelected = False
            if (space.name == settedPiece):
                image = Gtk.Image()
                image.name = "blank"
                space.set_image(image)
                space.isAvaible = True
            if (space.name == settedPiece and mode == False):
                image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/" + setted2PieceName + ".png")
                image.name =setted2PieceName
                space.set_image(image)
                space.isAvaible = False
                whiteTurn = False
    elif ("whiteking" in settedPieceName):
        image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/whiteking.png")
        image.name = "whiteking"

        widget.set_image (image)
        widget.isAvaible = False
        whiteTurn = False
        isWhiteKingMove = True
        for space in spaces:
            space.override_background_color(0,space.defaultColor)
            space.isSelected = False
            if (space.name == settedPiece):
                image = Gtk.Image()
                image.name = "blank"
                space.set_image(image)
                space.isAvaible = True
            if (widget.name == "G1" and settedPiece == "E1" and space.name == "F1"):
                image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/whiterook.png")
                image.name ="whiterook"
                space.set_image(image)
                space.isAvaible = False
                isWhiteRookDone = True
        if isWhiteRookDone:
            for space in spaces:
                if(space.name == "H1"):
                    image = Gtk.Image()
                    image.name = "blank"
                    space.set_image(image)
                    space.isAvaible = True
    elif ("blackking" in settedPieceName):
        image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/blackking.png")
        image.name = "blackking"

        widget.set_image (image)
        widget.isAvaible = False
        whiteTurn = True
        isBlackKingMove = True
        for space in spaces:
            space.override_background_color(0,space.defaultColor)
            space.isSelected = False
            if (space.name == settedPiece):
                image = Gtk.Image()
                image.name = "blank"
                space.set_image(image)
                space.isAvaible = True
            if (widget.name == "G8" and settedPiece == "E8" and space.name == "F8"):
                image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/blackrook.png")
                image.name ="blackrook"
                space.set_image(image)
                space.isAvaible = False
                isWhiteRookDone = True
        if isWhiteRookDone:
            for space in spaces:
                if(space.name == "H8"):
                    image = Gtk.Image()
                    image.name = "blank"
                    space.set_image(image)
                    space.isAvaible = True
    elif ("whiteknight" in settedPieceName):
        image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/whiteknight.png")
        if ("1" in settedPieceName):
            image.name = "whiteknight1"
        else:
            image.name = "whiteknight2"

        widget.set_image (image)
        widget.isAvaible = False
        whiteTurn = False
        for space in spaces:
            space.override_background_color(0,space.defaultColor)
            space.isSelected = False
            if (space.name == settedPiece):
                image = Gtk.Image()
                image.name = "blank"
                space.set_image(image)
                space.isAvaible = True
            if (space.name == settedPiece and mode == False):
                image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/" + setted2PieceName + ".png")
                image.name =setted2PieceName
                space.set_image(image)
                space.isAvaible = False
                whiteTurn = True
    elif ("blackknight" in settedPieceName):
        image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/blackknight.png")
        if ("1" in settedPieceName):
            image.name = "blackknight1"
        else:
            image.name = "blackknight2"

        widget.set_image (image)
        widget.isAvaible = False
        whiteTurn = True
        for space in spaces:
            space.override_background_color(0,space.defaultColor)
            space.isSelected = False
            if (space.name == settedPiece):
                image = Gtk.Image()
                image.name = "blank"
                space.set_image(image)
                space.isAvaible = True
            if (space.name == settedPiece and mode == False):
                image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/" + setted2PieceName + ".png")
                image.name =setted2PieceName
                space.set_image(image)
                space.isAvaible = False
                whiteTurn = False
    if ("whitepawn" in settedPieceName and widget.name[1]== "8"):
        image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/whitequeen.png")
        image.name = "whitequeen"
        widget.set_image (image)
        widget.isAvaible = False
        whiteTurn = False
    if ("blackpawn" in settedPieceName and widget.name[1]== "1"):
        image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/blackqueen.png")
        image.name = "blackqueen"
        widget.set_image (image)
        widget.isAvaible = False
        whiteTurn = True
    selectAttackAreas()
    if (is2nd == False):
        for space in spaces:
            if(space.name == settedPiece):
                settedPiece = widget.name
                settedPieceName = widget.get_image().name
                move (space,False)
def placement_pieces():
    global spaces
    for space in spaces:
        if (space.name == "E8"):
            image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/blackking.png")
            image.name = "blackking"

            space.set_image(image)
            space.isAvaible = False
        if (space.name == "D8"):
            image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/blackqueen.png")
            image.name = "blackqueen"

            space.set_image(image)
            space.isAvaible = False
        if (space.name == "A8" ):
            image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/blackrook.png")
            image.name = "blackrook1"

            space.set_image(image)
            space.isAvaible = False
        if (space.name == "H8" ):
            image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/blackrook.png")
            image.name = "blackrook2"

            space.set_image(image)
            space.isAvaible = False
        if (space.name == "C8" ):
            image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/blackbishop.png")
            image.name = "blackbishop1"

            space.set_image (image)
            space.isAvaible = False
            space.isWhite = False
        if (space.name == "F8" ):
            image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/blackbishop.png")
            image.name = "blackbishop2"

            space.set_image (image)
            space.isAvaible = False
            space.isWhite = False
        if (space.name == "B8" ):
            image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/blackknight.png")
            image.name = "blackknight1"

            space.set_image (image)
            space.isAvaible = False
        if (space.name == "G8" ):
            image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/blackknight.png")
            image.name = "blackknight2"

            space.set_image (image)
            space.isAvaible = False
        if ("7" in space.name):
            image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/blackpawn.png")
            image.name = "blackpawn"

            space.set_image (image)
            space.isAvaible = False
            space.isWhite = False
        if (space.name == "E1"):
            image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/whiteking.png")
            image.name ="whiteking"

            space.set_image(image)
            space.isAvaible = False
        if (space.name == "D1"):
            image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/whitequeen.png")
            image.name ="whitequeen"

            space.set_image(image)
            space.isAvaible = False
        if (space.name == "A1" ):
            image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/whiterook.png")
            image.name ="whiterook1"

            space.set_image(image)
            space.isAvaible = False
        if (space.name == "H1" ):
            image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/whiterook.png")
            image.name ="whiterook2"

            space.set_image(image)
            space.isAvaible = False
        if (space.name == "C1" ):
            image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/whitebishop.png")
            image.name ="whitebishop1"

            space.set_image (image)
            space.isAvaible = False
        if (space.name == "F1"):
            image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/whitebishop.png")
            image.name ="whitebishop2"

            space.set_image (image)
            space.isAvaible = False
        if (space.name == "B1" ):
            image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/whiteknight.png")
            image.name ="whiteknight1"

            space.set_image (image)
            space.isAvaible = False
        if (space.name == "G1" ):
            image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/whiteknight.png")
            image.name ="whiteknight2"

            space.set_image (image)
            space.isAvaible = False
        if ("2" in space.name):
            image = Gtk.Image.new_from_file(os.path.dirname(os.path.realpath(__file__)) + "/whitepawn.png")
            image.name = "whitepawn{}".format(space.name[0])

            space.set_image (image)
            space.isAvaible = False
class Space (Gtk.Button):
    isAvaible = True
    isSelected = False
    wayOfKing = False 
    wayOfStuck = False
    wayOfStuck1Rook = False
    wayOfStuck2Rook = False
    wayOfStuck1Knight = False
    wayOfStuck2Knight = False
    wayOfStuck1Bishop = False
    wayOfStuck2Bishop = False
    wayOfStuckQueen = False

    canWhiteAttack = False
    canBlackAttack = False

    can1WhiteKnightAttack = False
    can2WhiteKnightAttack = False

    can1BlackKnightAttack = False
    can2BlackKnightAttack = False

    can1WhiteRookAttackTop = False
    can1WhiteRookAttackBottom = False
    can1WhiteRookAttackRight = False
    can1WhiteRookAttackLeft = False

    can1WhiteRook2ndAttackTop = False
    can1WhiteRook2ndAttackBottom = False
    can1WhiteRook2ndAttackRight = False
    can1WhiteRook2ndAttackLeft = False

    can2WhiteRookAttackTop = False
    can2WhiteRookAttackBottom = False
    can2WhiteRookAttackRight = False
    can2WhiteRookAttackLeft = False

    can2WhiteRook2ndAttackTop = False
    can2WhiteRook2ndAttackBottom = False
    can2WhiteRook2ndAttackRight = False
    can2WhiteRook2ndAttackLeft = False

    can1BlackRookAttackTop = False
    can1BlackRookAttackBottom = False
    can1BlackRookAttackRight = False
    can1BlackRookAttackLeft = False

    can1BlackRook2ndAttackTop = False
    can1BlackRook2ndAttackBottom = False
    can1BlackRook2ndAttackRight = False
    can1BlackRook2ndAttackLeft = False

    can2BlackRookAttackTop = False
    can2BlackRookAttackBottom = False
    can2BlackRookAttackRight = False
    can2BlackRookAttackLeft = False

    can1WhiteBishopAttackTop = False
    can1WhiteBishopAttackBottom = False
    can1WhiteBishopAttackRight = False
    can1WhiteBishopAttackLeft = False

    can1WhiteBishop2ndAttackTop = False
    can1WhiteBishop2ndAttackBottom = False
    can1WhiteBishop2ndAttackRight = False
    can1WhiteBishop2ndAttackLeft = False

    can2WhiteBishopAttackTop = False
    can2WhiteBishopAttackBottom = False
    can2WhiteBishopAttackRight = False
    can2WhiteBishopAttackLeft = False
        
    can2WhiteBishop2ndAttackTop = False
    can2WhiteBishop2ndAttackBottom = False
    can2WhiteBishop2ndAttackRight = False
    can2WhiteBishop2ndAttackLeft = False
        
    can1BlackBishopAttackTop = False
    can1BlackBishopAttackBottom = False
    can1BlackBishopAttackRight = False
    can1BlackBishopAttackLeft = False

    can1BlackBishop2ndAttackTop = False
    can1BlackBishop2ndAttackBottom = False
    can1BlackBishop2ndAttackRight = False
    can1BlackBishop2ndAttackLeft = False

    can2BlackBishopAttackTop = False
    can2BlackBishopAttackBottom = False
    can2BlackBishopAttackRight = False
    can2BlackBishopAttackLeft = False

    can2BlackBishop2ndAttackTop = False
    can2BlackBishop2ndAttackBottom = False
    can2BlackBishop2ndAttackRight = False
    can2BlackBishop2ndAttackLeft = False

    canWhiteQueenAttackTop = False
    canWhiteQueenAttackTopRight = False
    canWhiteQueenAttackRight = False
    canWhiteQueenAttackBottomRight = False
    canWhiteQueenAttackBottom = False
    canWhiteQueenAttackBottomLeft = False
    canWhiteQueenAttackLeft = False
    canWhiteQueenAttackTopLeft = False

    canWhiteQueen2ndAttackTop = False
    canWhiteQueen2ndAttackTopRight = False
    canWhiteQueen2ndAttackRight = False
    canWhiteQueen2ndAttackBottomRight = False
    canWhiteQueen2ndAttackBottom = False
    canWhiteQueen2ndAttackBottomLeft = False
    canWhiteQueen2ndAttackLeft = False
    canWhiteQueen2ndAttackTopLeft = False

    canBlackQueenAttackTop = False
    canBlackQueenAttackTopRight = False
    canBlackQueenAttackRight = False
    canBlackQueenAttackBottomRight = False
    canBlackQueenAttackBottom = False
    canBlackQueenAttackBottomLeft = False
    canBlackQueenAttackLeft = False
    canBlackQueenAttackTopLeft = False

    canBlackQueen2ndAttackTop = False
    canBlackQueen2ndAttackTopRight = False
    canBlackQueen2ndAttackRight = False
    canBlackQueen2ndAttackBottomRight = False
    canBlackQueen2ndAttackBottom = False
    canBlackQueen2ndAttackBottomLeft = False
    canBlackQueen2ndAttackLeft = False
    canBlackQueen2ndAttackTopLeft = False

    stuck = False
    defaultColor = Gdk.RGBA()
    currentColor = Gdk.RGBA()
class MyWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Hello World")

        global colorDark
        global colorLight
        global spaces

        self.set_default_size(600,600)
        self.set_resizable(False)

        headerBar = Gtk.HeaderBar()
        headerBar.set_show_close_button(True)
        headerBar.props.title = "Chess"

        newGameButton = Gtk.Button()
        newGameButton.set_label("New Game")
        newGameButton.connect("clicked", self.on_newGameButton_clicked)
        headerBar.pack_start(newGameButton)

        preferenceButton = Gtk.Button()
        headerBar.pack_end(preferenceButton)

        self.set_titlebar(headerBar)

        self.fixed = Gtk.Fixed()
        self.add(self.fixed)

        for x in range(64):
            row = x / 8
            row = int(row)
            t = x % 8
            ch = 'A'
            column = chr(ord(ch) + t)
            z = column + "{}"
            self.space = Space()
            self.space.name = z.format (8 - row)
            self.space.set_size_request(75,75)
            self.space.set_alignment = 0
            self.space.set_padding = 0
            self.space.set_margin = 0
            image = Gtk.Image()
            image.name = "blank"
            self.space.set_image(image)

            if (row % 2 == 0):
                if (x % 2 == 0):
                    self.space.override_background_color(0,colorDark)
                    self.space.defaultColor = colorDark
                else:
                    self.space.override_background_color(0,colorLight)
                    self.space.defaultColor = colorLight
            else:
                if (x % 2 == 0):
                    self.space.override_background_color(0,colorLight)
                    self.space.defaultColor = colorLight
                else:
                    self.space.override_background_color(0,colorDark)
                    self.space.defaultColor = colorDark
            self.space.connect("clicked", self.on_space_clicked)

            self.fixed.put(self.space,t*75,row*75)
            spaces.append(self.space)
    def on_space_clicked(self, widget):
        global settedPiece
        global settedPieceName
        global movement
        global whiteTurn
        global colorSelected
        global breakPiece
        global isWhiteKingMove
        global isBlackKingMove
        global isWhiteRookMove
        global isBlackRookMove
        isFirstPosible = False
        isSecondPosible = False
        isThirdPosible = False
        global chess
        global spaces
        if (widget.isAvaible == False):
            if (widget.name == settedPiece):
                settedPiece = ""
                for space in spaces:
                    space.override_background_color(0,space.defaultColor)
                    space.isSelected = False
            elif ("whitepawn" in widget.get_image().name):
                if(whiteTurn):
                    a = "{}{}"
                    a = a.format(widget.name[0], chr(ord(widget.name[1]) +1 ))
                    b = "{}{}"
                    b = b.format(widget.name[0], chr(ord(widget.name[1]) + 2))
                    c = "{}{}"
                    c = c.format(chr(ord(widget.name[0]) + 1), chr(ord(widget.name[1]) + 1))
                    d = "{}{}"
                    d = d.format(chr(ord(widget.name[0]) - 1), chr(ord(widget.name[1]) + 1))
                    settedPiece = widget.name
                    settedPieceName = widget.get_image().name
                    for space in spaces:
                        space.override_background_color(0,space.defaultColor)
                        space.isSelected = False
                        if ((space.name == a and space.isAvaible) or (widget.name[1] == "2" and space.name == b and space.isAvaible) or (("black" in space.get_image().name) and ( space.name == c or space.name == d)) or space.name == widget.name ):
                            space.override_background_color(0,colorSelected)
                            space.isSelected = True
                    widget.override_background_color(0,colorSelected)
                if(whiteTurn == False and widget.isSelected):
                    move(widget,True)
            elif ("blackpawn" in widget.get_image().name):
                if (whiteTurn == False):
                    a = "{}{}"
                    a = a.format(widget.name[0], chr(ord(widget.name[1]) - 1))
                    b = "{}{}"
                    b = b.format(widget.name[0], chr(ord(widget.name[1]) - 2))
                    c = "{}{}"
                    c = c.format(chr(ord(widget.name[0]) + 1), chr(ord(widget.name[1]) - 1))
                    d = "{}{}"
                    d = d.format(chr(ord(widget.name[0]) - 1), chr(ord(widget.name[1]) - 1))
                    settedPiece = widget.name
                    settedPieceName = widget.get_image().name
                    for space in spaces:
                        space.override_background_color(0,space.defaultColor)
                        space.isSelected = False
                        if ((space.name == a and space.isAvaible) or (widget.name[1] == "7" and space.name == b and space.isAvaible) or (("white" in space.get_image().name) and ( space.name == c or space.name == d)) or space.name == widget.name ):
                            if (widget.stuck == False or chess == False  or ((widget.stuck and space.wayOfKing and chess == False))):
                                space.override_background_color(0,colorSelected)
                                space.isSelected = True
                    widget.override_background_color(0,colorSelected)
                if(whiteTurn and widget.isSelected):
                    move(widget,True)
            elif ("rook" in widget.get_image().name):
                if ((("white" in widget.get_image().name) and whiteTurn) or (("black" in widget.get_image().name) and (whiteTurn == False)) ):
                    settedPiece = widget.name
                    settedPieceName = widget.get_image().name
                    for space in spaces:
                            space.override_background_color(0,space.defaultColor)
                            space.isSelected = False
                    y = 8
                    for i in range (8):
                        for space in spaces:
                            ch = "{}{}"
                            ch = ch.format(widget.name[0] , chr(ord(widget.name[1]) + i + 1))
                            if (space.name == ch):
                                if (space.isAvaible == False):
                                    if ((("white" in widget.get_image().name) and ("black" in space.get_image().name)) or (("black" in widget.get_image().name) and ("white" in space.get_image().name))):
                                        space.override_background_color(0,colorSelected)
                                        space.isSelected = True
                                    y = i
                                    break
                                else:
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                        if (i == y):
                            break
                    y = 8
                    for i in range (8):
                        for space in spaces:
                            ch = "{}{}"
                            ch = ch.format(widget.name[0] , chr(ord(widget.name[1])-(i + 1)))
                            if (space.name == ch):
                                if (space.isAvaible == False):
                                    if ((("white" in widget.get_image().name) and ("black" in space.get_image().name)) or (("black" in widget.get_image().name) and ("white" in space.get_image().name))):
                                        space.override_background_color(0,colorSelected)
                                        space.isSelected = True
                                    y = i
                                    break
                                else:
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                        if (i == y):
                            break
                    y = 8
                    for i in range (8):
                        for space in spaces:
                            ch = "{}{}"
                            ch = ch.format(chr(ord(widget.name[0]) + i + 1), widget.name[1] )
                            if (space.name == ch):
                                if (space.isAvaible == False):
                                    if ((("white" in widget.get_image().name) and ("black" in space.get_image().name)) or (("black" in widget.get_image().name) and ("white" in space.get_image().name))):
                                        space.override_background_color(0,colorSelected)
                                        space.isSelected = True
                                    y = i
                                    break
                                else:
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                        if (i == y):
                            break
                        y = 8
                    for i in range (8):
                        for space in spaces:
                            ch = "{}{}"
                            ch = ch.format(chr(ord(widget.name[0]) - (i + 1)), widget.name[1] )
                            if (space.name == ch):
                                if (space.isAvaible == False):
                                    if ((("white" in widget.get_image().name) and ("black" in space.get_image().name)) or (("black" in widget.get_image().name) and ("white" in space.get_image().name))):
                                        space.override_background_color(0,colorSelected)
                                        space.isSelected = True
                                    y = i
                                    break
                                else:
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                        if (i == y):
                            break

                    widget.override_background_color(0,colorSelected)
                elif (widget.isSelected):
                    move(widget,True)
            elif ("bishop" in widget.get_image().name):
                if ((("white" in widget.get_image().name) and whiteTurn) or (("black" in widget.get_image().name) and (whiteTurn == False)) ):
                    settedPiece = widget.name
                    settedPieceName = widget.get_image().name
                    for space in spaces:
                            space.override_background_color(0,space.defaultColor)
                            space.isSelected = False
                    y = 8
                    for i in range (8):
                        for space in spaces:
                            ch = "{}{}"
                            ch = ch.format( chr(ord(widget.name[0]) + i + 1) , chr(ord(widget.name[1]) + i + 1))
                            if (space.name == ch):
                                if (space.isAvaible == False):
                                    if ((("white" in widget.get_image().name) and ("black" in space.get_image().name)) or (("black" in widget.get_image().name) and ("white" in space.get_image().name))):
                                        space.override_background_color(0,colorSelected)
                                        space.isSelected = True
                                    y = i
                                    break
                                else:
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                        if (i == y):
                            break
                    y = 8
                    for i in range (8):
                        for space in spaces:
                            ch = "{}{}"
                            ch = ch.format(chr(ord(widget.name[0]) - (i + 1)) , chr(ord(widget.name[1]) - (i + 1)))
                            if (space.name == ch):
                                if (space.isAvaible == False):
                                    if ((("white" in widget.get_image().name) and ("black" in space.get_image().name)) or (("black" in widget.get_image().name) and ("white" in space.get_image().name))):
                                        space.override_background_color(0,colorSelected)
                                        space.isSelected = True
                                    y = i
                                    break
                                else:
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                        if (i == y):
                            break
                    y = 8
                    for i in range (8):
                        for space in spaces:
                            ch = "{}{}"
                            ch = ch.format(chr(ord(widget.name[0]) + i + 1),chr(ord(widget.name[1]) - (i + 1)))
                            if (space.name == ch):
                                if (space.isAvaible == False):
                                    if ((("white" in widget.get_image().name) and ("black" in space.get_image().name)) or (("black" in widget.get_image().name) and ("white" in space.get_image().name))):
                                        space.override_background_color(0,colorSelected)
                                        space.isSelected = True
                                    y = i
                                    break
                                else:
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                        if (i == y):
                            break
                        y = 8
                    for i in range (8):
                        for space in spaces:
                            ch = "{}{}"
                            ch = ch.format(chr(ord(widget.name[0]) - (i + 1)),chr(ord(widget.name[1]) + i + 1) )
                            if (space.name == ch):
                                if (space.isAvaible == False):
                                    if ((("white" in widget.get_image().name) and ("black" in space.get_image().name)) or (("black" in widget.get_image().name) and ("white" in space.get_image().name))):
                                        space.override_background_color(0,colorSelected)
                                        space.isSelected = True
                                    y = i
                                    break
                                else:
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                        if (i == y):
                            break

                    widget.override_background_color(0,colorSelected)
                elif (widget.isSelected):
                    move(widget,True)
            elif ("queen" in widget.get_image().name):
                if ((("white" in widget.get_image().name) and whiteTurn) or (("black" in widget.get_image().name) and (whiteTurn == False)) ):
                    settedPiece = widget.name
                    settedPieceName = widget.get_image().name
                    for space in spaces:
                            space.override_background_color(0,space.defaultColor)
                            space.isSelected = False
                    y = 8
                    for i in range (8):
                        for space in spaces:
                            ch = "{}{}"
                            ch = ch.format( chr(ord(widget.name[0]) + i + 1) , chr(ord(widget.name[1]) + i + 1))
                            if (space.name == ch):
                                if (space.isAvaible == False):
                                    if ((("white" in widget.get_image().name) and ("black" in space.get_image().name)) or (("black" in widget.get_image().name) and ("white" in space.get_image().name))):
                                        space.override_background_color(0,colorSelected)
                                        space.isSelected = True
                                    y = i
                                    break
                                else:
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                        if (i == y):
                            break
                    y = 8
                    for i in range (8):
                        for space in spaces:
                            ch = "{}{}"
                            ch = ch.format(chr(ord(widget.name[0]) - (i + 1)) , chr(ord(widget.name[1]) - (i + 1)))
                            if (space.name == ch):
                                if (space.isAvaible == False):
                                    if ((("white" in widget.get_image().name) and ("black" in space.get_image().name)) or (("black" in widget.get_image().name) and ("white" in space.get_image().name))):
                                        space.override_background_color(0,colorSelected)
                                        space.isSelected = True
                                    y = i
                                    break
                                else:
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                        if (i == y):
                            break
                    y = 8
                    for i in range (8):
                        for space in spaces:
                            ch = "{}{}"
                            ch = ch.format(chr(ord(widget.name[0]) + i + 1),chr(ord(widget.name[1]) - (i + 1)))
                            if (space.name == ch):
                                if (space.isAvaible == False):
                                    if ((("white" in widget.get_image().name) and ("black" in space.get_image().name)) or (("black" in widget.get_image().name) and ("white" in space.get_image().name))):
                                        space.override_background_color(0,colorSelected)
                                        space.isSelected = True
                                    y = i
                                    break
                                else:
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                        if (i == y):
                            break
                    y = 8
                    for i in range (8):
                        for space in spaces:
                            ch = "{}{}"
                            ch = ch.format(chr(ord(widget.name[0]) - (i + 1)),chr(ord(widget.name[1]) + i + 1) )
                            if (space.name == ch):
                                if (space.isAvaible == False):
                                    if ((("white" in widget.get_image().name) and ("black" in space.get_image().name)) or (("black" in widget.get_image().name) and ("white" in space.get_image().name))):
                                        space.override_background_color(0,colorSelected)
                                        space.isSelected = True
                                    y = i
                                    break
                                else:
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                        if (i == y):
                            break
                    y = 8
                    for i in range (8):
                        for space in spaces:
                            ch = "{}{}"
                            ch = ch.format(widget.name[0] , chr(ord(widget.name[1]) + i + 1))
                            if (space.name == ch):
                                if (space.isAvaible == False):
                                    if ((("white" in widget.get_image().name) and ("black" in space.get_image().name)) or (("black" in widget.get_image().name) and ("white" in space.get_image().name))):
                                        space.override_background_color(0,colorSelected)
                                        space.isSelected = True
                                    y = i
                                    break
                                else:
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                        if (i == y):
                            break
                    y = 8
                    for i in range (8):
                        for space in spaces:
                            ch = "{}{}"
                            ch = ch.format(widget.name[0] , chr(ord(widget.name[1])-(i + 1)))
                            if (space.name == ch):
                                if (space.isAvaible == False):
                                    if ((("white" in widget.get_image().name) and ("black" in space.get_image().name)) or (("black" in widget.get_image().name) and ("white" in space.get_image().name))):
                                        space.override_background_color(0,colorSelected)
                                        space.isSelected = True
                                    y = i
                                    break
                                else:
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                        if (i == y):
                            break
                    y = 8
                    for i in range (8):
                        for space in spaces:
                            ch = "{}{}"
                            ch = ch.format(chr(ord(widget.name[0]) + i + 1), widget.name[1] )
                            if (space.name == ch):
                                if (space.isAvaible == False):
                                    if ((("white" in widget.get_image().name) and ("black" in space.get_image().name)) or (("black" in widget.get_image().name) and ("white" in space.get_image().name))):
                                        space.override_background_color(0,colorSelected)
                                        space.isSelected = True
                                    y = i
                                    break
                                else:
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                        if (i == y):
                            break
                    y = 8
                    for i in range (8):
                        for space in spaces:
                            ch = "{}{}"
                            ch = ch.format(chr(ord(widget.name[0]) - (i + 1)), widget.name[1] )
                            if (space.name == ch):
                                if (space.isAvaible == False):
                                    if ((("white" in widget.get_image().name) and ("black" in space.get_image().name)) or (("black" in widget.get_image().name) and ("white" in space.get_image().name))):
                                        space.override_background_color(0,colorSelected)
                                        space.isSelected = True
                                    y = i
                                    break
                                else:
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                        if (i == y):
                            break
                    widget.override_background_color(0,colorSelected)
                elif (widget.isSelected):
                    move(widget,True)
            elif ("king" in widget.get_image().name):
                if ((("white" in widget.get_image().name) and whiteTurn) or (("black" in widget.get_image().name) and (whiteTurn == False)) ):
                    settedPiece = widget.name
                    settedPieceName = widget.get_image().name
                    for space in spaces:
                            space.override_background_color(0,space.defaultColor)
                            space.isSelected = False
                    for space in spaces:
                        a = "{}{}"
                        a = a.format( widget.name[0] , chr(ord(widget.name[1]) + 1))
                        b = "{}{}"
                        b = b.format( widget.name[0] , chr(ord(widget.name[1]) - 1))
                        c = "{}{}"
                        c = c.format( chr(ord(widget.name[0]) + 1) , widget.name[1])
                        d = "{}{}"
                        d = d.format( chr(ord(widget.name[0]) - 1) , widget.name[1])
                        e = "{}{}"
                        e = e.format( chr(ord(widget.name[0]) + 1) , chr(ord(widget.name[1]) + 1))
                        f = "{}{}"
                        f = f.format( chr(ord(widget.name[0]) + 1) , chr(ord(widget.name[1]) - 1))
                        g = "{}{}"
                        g = g.format( chr(ord(widget.name[0]) - 1) , chr(ord(widget.name[1]) + 1))
                        h = "{}{}"
                        h = h.format( chr(ord(widget.name[0]) - 1) , chr(ord(widget.name[1]) - 1))
                        if ((space.name == a or space.name == b or space.name == c or space.name == d or space.name == e or space.name == f or space.name == g or space.name == h)):
                            if (("white" in widget.get_image().name and space.canBlackAttack == False) or ("black" in widget.get_image().name and space.canWhiteAttack == False)):

                                if (space.isAvaible == False):
                                    if ((("white" in widget.get_image().name) and ("black" in space.get_image().name)) or (("black" in widget.get_image().name) and ("white" in space.get_image().name))):
                                        space.override_background_color(0,colorSelected)
                                        space.isSelected = True
                                else:
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                    if (widget.name == "E1" and isWhiteKingMove == False and isWhiteRookMove == False):
                        for space in spaces:
                            if (space.name == "F1" and space.isAvaible):
                                isFirstPosible = True
                            if (space.name == "G1" and space.isAvaible):
                                isSecondPosible = True
                            if (space.name == "H1" and ("whiterook" in space.get_image().name)):
                                isThirdPosible = True
                        if (isFirstPosible and isSecondPosible and isThirdPosible):
                            for space in spaces:
                                if(space.name == "G1"):
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                    if (widget.name == "E8" and isBlackKingMove == False and isBlackRookMove == False):
                        for space in spaces:
                            if (space.name == "F8" and space.isAvaible):
                                isFirstPosible = True
                            if (space.name == "G8" and space.isAvaible):
                                isSecondPosible = True
                            if (space.name == "H8" and ("blackrook" in space.get_image().name)):
                                isThirdPosible = True
                        if (isFirstPosible and isSecondPosible and isThirdPosible):
                            for space in spaces:
                                if(space.name == "G8"):
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                    widget.override_background_color(0,colorSelected)
                elif (widget.isSelected):
                    move(widget,True)
            elif ("knight" in widget.get_image().name):
                if ((("white" in widget.get_image().name) and whiteTurn) or (("black" in widget.get_image().name) and (whiteTurn == False)) ):
                    settedPiece = widget.name
                    settedPieceName = widget.get_image().name
                    for space in spaces:
                            space.override_background_color(0,space.defaultColor)
                            space.isSelected = False 
                    for space in spaces:
                        a = "{}{}"
                        a = a.format( chr(ord(widget.name[0]) + 2) , chr(ord(widget.name[1]) + 1))
                        b = "{}{}"
                        b = b.format( chr(ord(widget.name[0]) + 2) , chr(ord(widget.name[1]) - 1))
                        c = "{}{}"
                        c = c.format( chr(ord(widget.name[0]) - 2) , chr(ord(widget.name[1]) + 1))
                        d = "{}{}"
                        d = d.format( chr(ord(widget.name[0]) - 2) , chr(ord(widget.name[1]) - 1))
                        e = "{}{}"
                        e = e.format( chr(ord(widget.name[0]) + 1) , chr(ord(widget.name[1]) + 2))
                        f = "{}{}"
                        f = f.format( chr(ord(widget.name[0]) + 1) , chr(ord(widget.name[1]) - 2))
                        g = "{}{}"
                        g = g.format( chr(ord(widget.name[0]) - 1) , chr(ord(widget.name[1]) + 2))
                        h = "{}{}"
                        h = h.format( chr(ord(widget.name[0]) - 1) , chr(ord(widget.name[1]) - 2))
                        if (space.name == a or space.name == b or space.name == c or space.name == d or space.name == e or space.name == f or space.name == g or space.name == h):
                            if (space.isAvaible == False):
                                if ((("white" in widget.get_image().name) and ("black" in space.get_image().name)) or (("black" in widget.get_image().name) and ("white" in space.get_image().name))):
                                    space.override_background_color(0,colorSelected)
                                    space.isSelected = True
                            else:
                                space.override_background_color(0,colorSelected)
                                space.isSelected = True
                    widget.override_background_color(0,colorSelected)
                elif (widget.isSelected):
                    move(widget,True)
            if (chess and "king"):
                for space in spaces:
                    if space.isSelected != space.wayOfKing:
                        space.override_background_color(0,space.defaultColor)
                        space.isSelected = False
                widget.override_background_color(0,colorSelected)
            if (chess and "king" in widget.get_image().name):
                for space in spaces:
                    if space.isSelected == space.wayOfKing:
                        space.override_background_color(0,space.defaultColor)
                        space.isSelected = False
            if (widget.stuck):
                for space in spaces:
                    if ("pawn" in widget.get_image().name):
                        if(space.isSelected != space.wayOfStuck):
                            space.override_background_color(0,space.defaultColor)
                            space.isSelected = False
                    if ("bishop1" in widget.get_image().name):
                        if(space.isSelected != space.wayOfStuck1Bishop):
                            space.override_background_color(0,space.defaultColor)
                            space.isSelected = False
                    if ("bishop2" in widget.get_image().name):
                        if(space.isSelected != space.wayOfStuck2Bishop):
                            space.override_background_color(0,space.defaultColor)
                            space.isSelected = False
                    if ("rook1" in widget.get_image().name):
                        if(space.isSelected != space.wayOfStuck1Rook):
                            space.override_background_color(0,space.defaultColor)
                            space.isSelected = False
                    if ("rook2" in widget.get_image().name):
                        if(space.isSelected != space.wayOfStuck2Rook):
                            space.override_background_color(0,space.defaultColor)
                            space.isSelected = False
                    if ("knight1" in widget.get_image().name):
                        if(space.isSelected != space.wayOfStuck1Knight):
                            space.override_background_color(0,space.defaultColor)
                            space.isSelected = False
                    if ("knight1" in widget.get_image().name):
                        if(space.isSelected != space.wayOfStuck2Knight):
                            space.override_background_color(0,space.defaultColor)
                            space.isSelected = False
                    if ("queen" in widget.get_image().name):
                        if(space.isSelected != space.wayOfStuckQueen):
                            space.override_background_color(0,space.defaultColor)
                            space.isSelected = False
        else:
            if (widget.isSelected == True ):
                move(widget,True)
    def on_newGameButton_clicked(self,widget):
        global spaces
        global whiteTurn
        for space in spaces:
            space.isAvaible = True
            space.isSelected = False
            space.canWhiteAttack = False
            space.canWhite2ndAttack = False
            space.canBlackAttack = False
            space.canBlack2ndAttack = False
            space.stuck = False
            space.override_background_color(0,space.defaultColor)
            image = Gtk.Image()
            image.name = "blank"
            space.set_image(image)
        whiteTurn = True
        placement_pieces()
win = MyWindow()
placement_pieces()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()